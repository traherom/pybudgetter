#!/usr/bin/env python3
import sys
import argparse

from PyQt5.QtCore import QObject

from pybudgetter.budget.manager import BudgetManager
from pybudgetter.scanner.scanner import OnlineTransactionScanner

class SignalPrinter(QObject):
    def __init__(self):
        super(SignalPrinter, self).__init__()

    def print_msg(self, msg):
        print(msg)

def main(argv):
    parser = argparse.ArgumentParser(description='Work with a SQLite-based budget')
    parser.add_argument('db', help='Path to a budget database')
    args = parser.parse_args()

    status_printer = SignalPrinter()

    budget = BudgetManager()
    budget.open(args.db)
    scanner = OnlineTransactionScanner(budget)
    scanner.scan_status.connect(status_printer.print_msg)
    scanner.scan_all_logins()

    return 0

if __name__ == '__main__':
    sys.exit(main(sys.argv))

