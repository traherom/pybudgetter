.PHONY : client

all : client

run : client
	./budgetter.py

debug : client
	python -c 'import pdb; import budgetter; pdb.run("budgetter.main()")'
	
client :
	$(MAKE) -C pybudgetter

clean :
	$(MAKE) -C pybudgetter clean
