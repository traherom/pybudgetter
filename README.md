# README #

PyBudgetter is a Python-based budgetting tool that uses no central server and hence never needs to release personal information. All the basic features exist, such as creating budget categories and automatically grabbing transactions from banks. Currently, only USAA accounts are supported although more are planned. If you have a particular bank request, submit an issue.

All information is stored in a local SQLite database but is currently not encrypted in any way. If sharing amongst multiple people (IE, family members all looking at the same budget), it's suggested you sync via something like Dropbox. PyBudgetter locks the database to ensure only one person opens it a at a time.

### Dependencies ###
Currently, PyBudgetter depends on the following libraries:

- [Python 3+](https://www.python.org/)
- [Qt5](https://qt-project.org/wiki/Qt_5.0)
- [PyQt5](http://www.riverbankcomputing.co.uk/software/pyqt/download5)
- [ofxparse](https://pypi.python.org/pypi/ofxparse/0.14)

### How do I get set up? ###

1. Install the dependencies listed above.

2. Clone this repository:
```
git clone git@bitbucket.org:traherom/pybudgetter.git
```

3. Run make in the cloned repository:
```
cd pybudgetter
make
```

4. Run PyBudgetter:
```
./budgetter.py
```