import re
import datetime
import importlib

from pprint import pprint
import traceback

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject

import urllib.request, urllib.error, urllib.parse
from urllib.parse import urlencode
from http.cookiejar import CookieJar

from ..helpers import logins
from ..errors import BudgetNoSuchRowError
from . import errors
from .. import utilities

class ScannerPlugin(QObject):
    # Signals
    scan_status = pyqtSignal(str)

    def __init__(self, scanner):
        super(ScannerPlugin, self).__init__()

        self.scanner = scanner
        self.budget = scanner.get_budget()
        self.__curr_login = None
        self.__driver = None
        self.__logged_in = False

        self.__cookies = CookieJar()

    def __del__(self):
        # Close webdriver if needed
        self.close_driver()
        pass

    def get_plugin_name(self):
        """Override in plugin to return a pretty name of the plugin"""
        raise NotImplementedError('login() not implemented by plugin')

    #######################
    # Scanner state
    def set_current_login(self, login):
        """Selects the current login ID, used by other methods in class"""
        # Reset everything if the login changes
        if login != self.__curr_login:
            self.clear_cookies()

        self.__curr_login = login

    def set_logged_in(self, is_logged_in):
        """Sets whether  or not this plugin believes it is logged in"""
        self.__logged_in = is_logged_in

    def is_logged_in(self):
        """Returns whether this plugin believes it is logged in"""
        return self.__logged_in

    #######################
    # Control for user
    def run(self, login_id):
        """Default run functionality logs in, gets account list, and gets all
        transactions for each account. May be overridden by plugins if desired.
        Any ScannerFailureErrors thrown by called functions are recorded as the
        login error message."""
        # Don't go further if we know we need more info
        if self.get_needed_settings():
            raise ScannerUserDataNeededError('Settings needed for this login')

        # Ensure we have a clean start
        self.close_driver()
        self.set_current_login(login_id)
        self.clear_error_msg()

        name = self.get_plugin_name()
        self.scan_status.emit('Beginning scan for {} login'.format(name))

        try:
            self.scan_status.emit('Logging in to {}'.format(name))
            self.login()

            self.scan_status.emit('Getting accounts under {} login'.format(name))
            accounts = self.get_accounts()
            for aid, url in accounts.items():
                self.scan_status.emit('Fetching transactions under {} account {}'.format(name, aid))
                self.get_account_transactions(aid, url, max_days=60)
        except errors.ScannerFailureError as e:
            self.set_error_msg(e.msg)
            self.scan_status.emit('Error occurred scanning {}'.format(name))
            raise e

        # If we get here, there are no errors to report!
        self.scan_status.emit('Scan of {} completed successfully!'.format(name))
        #self.clear_error_msg()

    def login(self):
        """Should be overridden by the plugin"""
        raise NotImplementedError('login() not implemented by plugin')

    def get_accounts(self):
        """Should be overridden by the plugin"""
        raise NotImplementedError('get_accounts() not implemented by plugin')

    def get_account_transactions(self, account_id, url, max_days):
        """Should be overridden by the plugin"""
        raise NotImplementedError('get_account_transactions() not implemented by plugin')

    #######################
    # Scanner settings/state
    # Standard keys
    def setting(self, key, value=None):
        """Sets or gets the value for the given login/key pair"""
        if value is not None:
            setting = self.budget.create_login_setting(self.__curr_login, key)
            setting.set_value(value)
            setting.set_needed(False)
            return value
        else:
            try:
                setting = self.budget.get_login_setting(login_id=self.__curr_login, key=key)
                return setting.get_value()
            except BudgetNoSuchRowError:
                return None

    def request_setting(self, key, prompt=None):
        """Marks a setting as needed and gives it a prompt. If no prompt is given,
        the key is used."""
        # Get current prompt and value for setting (assuming it exists)
        setting = self.budget.create_login_setting(self.__curr_login, key)
        curr_prompt = setting.get_prompt()

        # Choose the best prompt, preferring specified prompt or standard prompt based on the key
        if prompt is None:
            if key == logins.BudgetLoginSetting.USERNAME_KEY:
                prompt = "Username"
            elif key == logins.BudgetLoginSetting.PASSWORD_KEY:
                prompt = "Password"
            elif key == logins.BudgetLoginSetting.PIN_KEY:
                prompt = "PIN"
            elif curr_prompt:
                prompt = curr_prompt
            else:
                prompt = key

        # Mark as needed and add the prompt.
        setting.set_needed(True)
        setting.set_prompt(prompt)

    def get_needed_settings(self):
        """Returns a dict of any needed settings for this login and the prompted value"""
        return self.budget.get_needed_login_settings(self.__curr_login)

    def text_question(self, question, answer=None):
        """Sets or gets the answer to the given question"""
        question = question.strip()
        if answer is None:
            return self.setting('Q:{}'.format(question))
        else:
            return self.setting('Q:{}'.format(question), answer)

    def request_question(self, question):
        """Requests the answer to the given question"""
        self.request_setting('Q:{}'.format(question), question)

    def set_error_msg(self, msg):
        """Saves an error message to display to the user for this login"""
        self.setting(logins.BudgetLoginSetting.ERROR_KEY, msg)

    def clear_error_msg(self):
        """Clears any existing error message for this login"""
        self.setting(logins.BudgetLoginSetting.ERROR_KEY, '')

    def cursor(self):
        """Returns a database cursor from the scanner's DB"""
        return self.scanner.db.cursor()

    def commit(self):
        """Commits current transaction"""
        return self.scanner.db.commit()

    #######################
    # Accounts
    def get_account_id(self, name):
        """Get the ID of a bank account given a name (based on the current login)"""
        try:
            return self.budget.get_account_id(self.__curr_login, name)
        except BudgetNoSuchRowError:
            return None

    def create_account(self, name):
        """Creates a new account under the current login"""
        return self.budget.create_account(self.__curr_login, name)

    #######################
    # Transactions
    def get_latest_transaction(self, account_id):
        """Gets the newest transaction for the given account"""
        try:
            return self.budget.get_latest_transaction(account_id)
        except BudgetNoSuchRowError:
            return None

    def insert_transaction(self, account_id, date, desc, amount):
        """Takes the given transaction and saves it to database"""
        return self.budget.create_transaction(account_id, date, desc, amount)

    def compare_transactions(self, t1, t2_account_id=None, t2_date=None, t2_desc=None, t2_amount=None, t2_cat=None):
        """Returns True if an existing transaction (t1) is the same as the given values for another
        transaction (t2). Only examines given t2 values."""
        if t2_account_id is not None and t1.get_account_id() != t2_account_id:
            return False
        elif t2_date is not None and t1.get_date() != t2_date:
            return False
        elif t2_desc is not None and t1.get_description() != t2_desc:
            return False
        elif t2_amount is not None and t1.get_amount() != t2_amount:
            return False
        elif t2_cat is not None and t2.get_category_id() != t2_cat:
            return False
        else:
            return True

    #######################
    # Scraping helpers
    def create_driver(self, visible=False):
        """Creates a new webdriver for scraping"""
        # Do the import here to allow scanner to load and not fail until a  plugin
        # that actually uses selenium tries to start
        try:
            from selenium import webdriver
            from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
        except ImportError:
            raise errors.ScannerPackageMissingError('Selenium not found, unable to use plugin')

        self.close_driver()

        if visible:
            self.__driver = webdriver.Chrome()
        else:
            dcap = dict(DesiredCapabilities.PHANTOMJS)
            dcap["phantomjs.page.settings.userAgent"] = (
                "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1833.5 Safari/537.36"
            )

            self.__driver = webdriver.PhantomJS(desired_capabilities=dcap)

        return self.__driver

    def get_driver(self, visible=False):
        """Gets current webdriver for scraping/creates new if needed"""
        if self.__driver is None:
            self.create_driver(visible)
        return self.__driver

    def close_driver(self):
        """Quits current webdriver if there is one (no effect if none)"""
        if self.__driver is not None:
            self.__driver.quit()
            self.__driver = None

    def clear_cookies(self):
        """Clear all cookies for this scanner"""
        self.__cookies.clear()

    def print_cookies(self):
        """Displays current cookies for debugging purposes"""
        for c in self.__cookies:
            print(c)

    def __set_user_agent(self, req):
        """Adds the Chrome header to the request"""
        req.add_header('User-Agent',
            'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_1) '
            'AppleWebKit/537.36 (KHTML, like Gecko) Chrome/34.0.1809.0 Safari/537.36')
        return req

    def get(self, url, params=None):
        """Does a GET for the given URL, along with any set cookies"""
        # Build request with cookies and data
        # To keep it a GET, we manipulate the URL manually in advance
        if params is not None:
            url_params = urlencode(params)
            url = ''.join(url, '?', url_params)
            print('added params {}'.format(url))

        req = urllib.request.Request(url)
        self.__set_user_agent(req)
        self.__cookies.add_cookie_header(req)

        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.__cookies))
        resp = opener.open(req)

        return resp

    def post(self, url, params=None):
        """Does a POST for the given URL, along with any set cookies"""
        # Build request with cookies and data
        req = urllib.request.Request(url)
        self.__set_user_agent(req)
        self.__cookies.add_cookie_header(req)

        if params is not None:
            url_params = urlencode(params)
            req.add_data(url_params)

        opener = urllib.request.build_opener(urllib.request.HTTPCookieProcessor(self.__cookies))
        resp = opener.open(req)

        return resp

def load(plugin_name):
    """Import the given plugin"""
    try:
        plugin_module = importlib.import_module('.{}'.format(plugin_name), package=__package__)
        return plugin_module
    except ImportError as e:
        raise errors.ScannerPackageMissingError('{}, unable to use plugin'.format(e))
