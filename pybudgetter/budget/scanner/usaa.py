from pprint import pprint
from datetime import datetime, timedelta
from time import sleep
import re

from selenium.common.exceptions import NoSuchElementException

from .scanner_plugin import ScannerPlugin
from .. import utilities, helpers
from . import errors

class UsaaPlugin(ScannerPlugin):
    def __init__(self, scanner):
        super(UsaaPlugin, self).__init__(scanner)

        # Regex for parsing transaction lines
        # Example 1: 02/14/2014   USAA FUNDS TRANSFER CR   $250.00
        # Example 2: 01/27/2014  PRINCIPAL   $181.90
        # Example 3: 12/30/2013   USAA FUNDS TRANSFER DB TO ******9388   ($1,000.00)
        self.__transaction_regex = re.compile(
                r"""^\s*                        # Opening whitespace
                    (\d\d/\d\d/\d{4})\s+        # Date
                    (.*)\s+                     # Description
                    (\(?\$[\d,]+\.\d\d\)?)      # Amount
                    \s*$                        # Ending whitespace
                    """, re.X)

    def get_plugin_name(self):
        return 'USAA'

    def login(self):
        """Ensures browser is logged in"""
        # Get username, password
        user = self.setting(helpers.BudgetLoginSetting.USERNAME_KEY)
        password = self.setting(helpers.BudgetLoginSetting.PASSWORD_KEY)
        if user is None or password is None:
            self.request_setting(helpers.BudgetLoginSetting.USERNAME_KEY)
            self.request_setting(helpers.BudgetLoginSetting.PASSWORD_KEY)
            raise errors.ScannerUserDataNeededError('Username/password needed')

        # Initial login
        self.scan_status.emit('Beginning login to USAA for user {}'.format(user))
        d = self.get_driver(visible=True)
        d.get('http://mobile.usaa.com')

        d.find_element_by_xpath("//a[contains(text(), 'Log On')]").click()

        self.scan_status.emit('Entering user name and password')
        username_el = d.find_element_by_name('j_username')
        username_el.send_keys(user)
        d.find_element_by_name('j_password').send_keys(password)
        username_el.submit()

        # Failure check
        if d.current_url.startswith('https://mobile.usaa.com/inet/ent_logon/Logon'):
            # What's the message?
            msg = d.find_element_by_xpath("//p[@class='notice_logonError']").text

            self.request_setting(helpers.BudgetLoginSetting.PASSWORD_KEY)
            if msg.find('locked') != -1:
                raise errors.ScannerUserDataNeededError('Account may be locked. Username/password rejected')
            else:
                raise errors.ScannerUserDataNeededError('Username/password incorrect')

        # PIN
        if d.current_url.startswith('https://mobile.usaa.com/inet/ent_auth_pin/page/PinEntryPage'):
            pin = self.setting(helpers.BudgetLoginSetting.PIN_KEY)
            if pin is None:
                self.request_setting(helpers.BudgetLoginSetting.PIN_KEY)
                raise errors.ScannerUserDataNeededError('PIN needed')

            self.scan_status.emit('Entering PIN')
            pin_el = d.find_element_by_id('id3')
            pin_el.send_keys(pin)
            pin_el.submit()

            # Failure check
            if d.current_url.startswith('https://mobile.usaa.com/inet/ent_auth_pin/page/PinEntryPage/'):
                self.request_setting(helpers.BudgetLoginSetting.PIN_KEY)
                raise errors.ScannerUserDataNeededError('PIN incorrect')

        # Questions
        if d.current_url.startswith('https://mobile.usaa.com/inet/ent_auth_secques'):
            # What's the question?
            content_xpath = "//div[@class='page-content']"
            a_input_el = d.find_element_by_xpath("{}//input[@type='text']".format(content_xpath))
            a_name = a_input_el.get_attribute('id')
            question = d.find_element_by_xpath("//label[@for='{}']".format(a_name)).text

            # Do we have an answer for it?
            answer = self.text_question(question)
            if answer is None:
                self.scan_status.emit('Need answer to "{}"'.format(question))
                self.request_question(question)
                raise errors.ScannerUserDataNeededError('Security question answer needed')

            self.scan_status.emit('Answering question "{}"'.format(question))
            a_input_el.send_keys(answer)
            a_input_el.submit()

            # Failure check
            if d.current_url.startswith('https://mobile.usaa.com/inet/ent_auth_secques/'):
                self.request_question(question)
                raise errors.ScannerUserDataNeededError('Security question answer incorrect')

        self.set_logged_in(True)
        return True

    def get_accounts(self):
        """Gets a list of all current accounts on this login and the URLs to access them"""
        d = self.get_driver()

        # Must be logged in
        if not self.is_logged_in():
            raise errors.ScannerPluginBrokenError('Attempt to get accounts while not logged in')

        # Navigate to accounts list
        if d.current_url.startswith('https://mobile.usaa.com/inet/ent_home/MbCpHome'):
            self.scan_status.emit('Switching to accounts list')
            d.get('https://mobile.usaa.com/inet/ent_manageAccounts/ManageAccountsApp/')

        # Grab the links to all of them, then process them one after the other
        self.scan_status.emit('Copying all account links')
        account_names = d.find_elements_by_xpath("//form[@id='id1']//li[@class='acct-group-row']/a")
        accounts = dict()
        for account in account_names:
            name = account.text.strip().split('\n')[0]
            aid = self.get_account_id(name)
            link = account.get_attribute('href')

            if aid is None:
                # Account unknown right now, need to create it
                self.scan_status.emit('Registering new account {}'.format(name))
                aid = self.create_account(name)

            self.scan_status.emit('Grabbed {}'.format(name))
            accounts[aid] = link

        return accounts

    def get_account_transactions(self, account_id, url, max_days):
        """Saves off all transactions since the last-known transaction/last max_days days"""
        # Must be logged in
        if not self.is_logged_in():
            raise errors.ScannerPluginBrokenError('Attempt to get accounts while not logged in')

        # Get how far back in the past we're going
        latest_trans = self.get_latest_transaction(account_id)
        furthest_date = datetime.today() - timedelta(days=max_days)
        if latest_trans is not None:
            furthest_date = latest_trans.get_date()

        # Start downloading, go to first page of account
        self.scan_status.emit('Fetching transactions back through {} for account {}'.format(furthest_date, account_id))
        d = self.get_driver()
        d.get(url)

        saved_num = 0
        retrieval_complete = False
        while not retrieval_complete:
            trans_lines = d.find_elements_by_xpath("//div[@class='details']")
            for line in trans_lines:
                # Ignore lines we can't parse, we're going to pick up some stuff we didn't want
                m = self.__transaction_regex.match(line.text.strip())
                if m is not None:
                    tdate = date=datetime.strptime(m.group(1), '%m/%d/%Y')
                    tdesc = m.group(2)
                    tamt = utilities.parse_dollars(m.group(3))

                    # Skip pending transactions
                    if tdesc.lower().find('pending') != -1:
                        continue

                    # Is this too far in the past/we already have it?
                    if furthest_date > tdate:
                        self.scan_status.emit('Max history reached')
                        retrieval_complete = True
                        break
                    if latest_trans is not None and self.compare_transactions(
                            t1=latest_trans,
                            t2_date=tdate,
                            t2_desc=tdesc,
                            t2_amount=tamt):
                        self.scan_status.emit('Found latest transaction, stopping retrieval')
                        retrieval_complete = True
                        break

                    # Save transaction
                    self.insert_transaction(account_id=account_id, date=tdate, desc=tdesc, amount=tamt)
                    saved_num += 1

            # Next page of transactions
            if not retrieval_complete:
                try:
                    d.find_element_by_xpath("//input[@value='Next']").click()
                except NoSuchElementException:
                    retrieval_complete = True
                    self.scan_status.emit('Stopping retrieval, next button not found (end of history?)')

        self.scan_status.emit('Saved {} new transactions'.format(saved_num))

def create(scanner):
    return UsaaPlugin(scanner)
