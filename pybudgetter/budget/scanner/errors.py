class ScannerFailureError(Exception):
    def __init__(self, msg=None):
        self.msg = msg

class ScannerUserDataNeededError(ScannerFailureError):
    pass

class ScannerPluginBrokenError(ScannerFailureError):
    pass

class ScannerPackageMissingError(ScannerFailureError):
    pass
