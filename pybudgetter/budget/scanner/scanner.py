import sys
import queue

from PyQt5.QtCore import pyqtSignal, pyqtSlot, QObject, QThread

from .scanner_plugin import load as load_scanner
from .errors import ScannerFailureError

class OnlineTransactionScanner(QObject):
    # Signals
    queue_empty = pyqtSignal()
    scan_started = pyqtSignal()
    scan_complete = pyqtSignal()
    scan_status = pyqtSignal(str)

    def __init__(self, budget):
        super(OnlineTransactionScanner, self).__init__()

        self.__budget = budget

        self.__do_async = False
        self.__main_thread = self.thread()
        self.__async_thread = None
        self.__async_queue = queue.Queue()

    def set_asynchronous(self, should_async):
        """Controls if scans are performed in the background. When complete,
        scans always emit scan_complete"""
        if self.__do_async == should_async:
            return

        self.__do_async = should_async

        if self.__do_async:
            if self.__async_thread is None:
                self.__async_thread = QThread()

            print('Swapped to asynchronous scanning')
            self.moveToThread(self.__async_thread)
            self.__async_thread.started.connect(self.__async_processor)
            self.__async_thread.start()
        else:
            print('Swapped to foreground scanning')
            #self.moveToThread(self.__main_thread)

    def __async_processor(self):
        """If a job is dropped in the async queue, runs it"""
        print('Async processing ready')
        while self.__do_async:
            try:
                login_id = self.__async_queue.get(timeout=5)
                self.__perform_scan(login_id)
                self.__async_queue.task_done()
            except queue.Empty:
                self.queue_empty.emit()

    def clear_async_queue(self):
        """Clears any existing items in the background scan queue"""
        # Dump any remaining items in the queue
        try:
            while True:
                self.__async_queue.get_nowait()
                self.__async_queue.task_done()
        except queue.Empty:
            pass

        # Ensure that the background thread finishes up if it was already
        # processing something
        self.__async_queue.join()
        self.queue_empty.emit()

    def get_plugin_list(self):
        """Returns a list of all available scanner plugins"""
        return ('usaa')

    def get_budget(self):
        """Returns the budget being used by this scanner"""
        return self.__budget

    @pyqtSlot(str)
    def on_plugin_status(self, msg):
        """Passes messages from plugins on to users of the scanner"""
        self.scan_status.emit(msg)

    def __perform_scan(self, login_id):
        """Updates the given login and corresponding accounts with transaction data
        from the website"""
        try:
            self.scan_started.emit()

            # Load plugin and pass off control
            plugin_name = self.__budget.get_login_plugin(login_id)
            self.scan_status.emit('Loading {} plugin'.format(plugin_name))
            p = load_scanner(plugin_name)

            instance = p.create(self)
            instance.scan_status.connect(self.on_plugin_status)

            # Run
            self.__budget.start_transaction()
            instance.run(login_id)
            self.__budget.commit()
        except ScannerFailureError as e:
            print('Failed to scan account: {}'.format(e.msg))
            self.__budget.rollback()
        finally:
            self.scan_complete.emit()

    def scan_login(self, login_id):
        """Scans given login and/or queues it to be scanned, based on
        whether asynchronous scanning is on or not"""
        print('Adding login {} to the queue'.format(login_id))
        if self.__do_async:
            self.__async_queue.put(login_id)
        else:
            self.__perform_scan(login_id)

    def scan_all_logins(self):
        """Updates all logins/accounts with web transactions"""
        for login_id in self.__budget.get_all_login_ids():
            self.scan_login(login_id)
