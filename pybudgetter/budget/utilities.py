import locale
from datetime import datetime

from PyQt5.QtGui import QBrush, QColor

#################
# Debug tool to show database models
def print_model_contents(model, lbl=''):
    """
    Debug function, prints contents of a model
    """
    print('---data in model: {}---'.format(lbl))
    for r in range(model.rowCount()):
        for c in range(model.columnCount()):
            idx = model.createIndex(r, c)
            print('{}\t'.format(model.data(idx, Qt.DisplayRole)), end='')
        print()
    print('---end data---')

######################
# Misc utility functions
def parse_iso_date(strdate):
    return datetime.strptime(strdate, '%Y-%m-%d %H:%M:%S')

def parse_dollars(amount):
    """Takes the given amount and returns a float representing the dollar amount"""
    if type(amount) == str or type(amount) == str:
        amount = amount.strip()
        amt = 0

        # Is it negative?
        neg = False
        if amount.find('-') != -1 or amount[0] == '(':
            neg = True

        # Remove everything except the number itself
        # TBD better way to do this?
        chars = list()
        for x in amount:
            if x in '0123456789.':
                chars.append(x)

        # Convert
        amt = float(''.join(chars))
        if not neg:
            return amt
        else:
            return -amt
    else:
        return float(amount)

def string_dollars(amount):
    """Takes the given amount, typically a float or int, and returns
    an accounting-style string. IE, -4005.12 becomes "-($4,005.12)"
    and 2.23 becomes "$2.23" """
    try:
        amount = float(amount)
    except ValueError:
        amount = 0.0

    with_commas = locale.format('%.2f', abs(amount), grouping=True)
    if amount < 0:
        return '-(${})'.format(with_commas)
    else:
        return '${}'.format(with_commas)

def dollars_brush(amt):
    """
    Returns an appropriate brush for the given dollar amount. IE,
    positive amounts are green, negative red.
    """
    try:
        amt = float(amt)
    except ValueError:
        amt = 0.0

    # Green positive amounts, red negative
    if amt < 0:
        return QBrush(QColor('red'))
    else:
        return QBrush(QColor('green'))
