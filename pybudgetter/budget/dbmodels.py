from pprint import pprint

from PyQt5.QtCore import Qt, QModelIndex
from PyQt5.QtSql import QSqlQueryModel

from .errors import BudgetError

class LoginModel(QSqlQueryModel):
    def __init__(self, budget):
        super(LoginModel, self).__init__()

        self.__budget = budget

        self.reinit_model()
        self.__budget.logins_changed.connect(self.reinit_model)

    @property
    def id_col(self):
        """Returns the column number of the login ID"""
        return 1

    def columnCount(self, parent=None):
        return 3

    def rowCount(self, parent=QModelIndex()):
        if self.__budget.is_open():
            return super(LoginModel, self).rowCount(parent)
        else:
            return 0

    def reinit_model(self):
        if not self.__budget.is_open():
            self.setQuery('')
            return

        query = self.__budget.query()
        if not query.prepare("""SELECT plugin||' - '||ls.value AS longname, logins.id, logins.plugin, ls.value
                                      FROM logins
                                      LEFT JOIN login_settings ls ON ls.key='user' AND logins.id=ls.login_id
                                      ORDER BY longname DESC"""):
            raise BudgetError('Failed to create LoginModel', query.lastError())
        if not query.exec_():
            raise BudgetError('Failed to create LoginModel', query.lastError())

        self.setQuery(query)
        #print_model_contents(self, 'Login')

class LoginSettingsModel(QSqlQueryModel):
    def __init__(self, budget):
        super(LoginSettingsModel, self).__init__()

        self.__budget = budget
        self.__login_id = None
        self.clear_login_id()

        self.__budget.logins_changed.connect(self.reinit_model)

    @property
    def id_col(self):
        """Returns the column number for the setting ID"""
        return 0

    def clear_login_id(self):
        """Causes the model to display no rows"""
        self.set_login_id(-1)

    def set_login_id(self, login_id):
        """Sets the current login ID for this model and refreshes the model data """
        if self.__login_id != login_id:
            self.__login_id = login_id
            self.reinit_model()

    @property
    def value_col(self):
        """Returns the index of the column containing the setting value """
        return 3

    def columnCount(self, parent=None):
        return 6

    def rowCount(self, parent=QModelIndex()):
        if self.__budget.is_open():
            return super(LoginSettingsModel, self).rowCount(parent)
        else:
            return 0

    def flags(self, index):
        """Mark relevant settings as editable"""
        if index.column() == self.value_col:
            return Qt.ItemIsEnabled | Qt.ItemIsEditable | Qt.ItemIsSelectable
        else:
            return super(LoginSettingsModel, self).flags(index)

    def data(self, index, role=Qt.DisplayRole):
        """Returns data as editable"""
        return super(LoginSettingsModel, self).data(index, role)

    def setData(self, index, value, role=Qt.EditRole):
        """Sets editable parts of a login setting"""
        if index.column() == self.value_col and role == Qt.EditRole:
            setting_id = self.data(self.index(index.row(), self.id_col))
            setting = self.__budget.get_login_setting(setting_id=setting_id)
            setting.set_value(value)
            setting.set_needed(False)
            return True
        else:
            return super(LoginSettingsModel, self).setData(index, value, role)

    def reinit_model(self):
        if not self.__budget.is_open() or self.__login_id is None:
            self.setQuery('')
            return

        query = self.__budget.query()
        if not query.prepare("""SELECT id, login_id, key, value, prompt, needed
                                    FROM login_settings WHERE login_id=?
                                    ORDER BY id ASC"""):
            raise BudgetError('Failed to create LoginSettingsModel', query.lastError())

        query.addBindValue(self.__login_id)

        if not query.exec_():
            raise BudgetError('Failed to create LoginSettingsModel', query.lastError())

        self.setQuery(query)
        #print_model_contents(self, 'LoginSettings (id {})'.format(self.__login_id))

