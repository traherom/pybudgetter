from PyQt5.QtSql import QSqlError

class BudgetError(Exception):
    """
    Exception raised by Budget (and its helpers)
    """
    def __init__(self, outer_msg, inner_msg=None):
        if inner_msg is None:
            super(Exception, self).__init__(str(outer_msg))
        elif isinstance(inner_msg, QSqlError):
            super(Exception, self).__init__(str(outer_msg), '{}: {}'.format(
                inner_msg.databaseText(), inner_msg.driverText()))
        else:
            super(Exception, self).__init__('{}: {}'.format(str(outer_msg), str(inner_msg)))

class BudgetVersionError(BudgetError):
    """
    Exception to denote a budget is an older type and needs to be upgraded
    """
    pass

class BudgetNotOpenError(BudgetError):
    """
    Exception to denote that an action cannot occure because no budget is opened
    """
    pass

class BudgetNoSuchRowError(BudgetError):
    """
    Exception to denote when a requested database row can not be found
    """
    pass

class BudgetLockedError(BudgetError):
    """
    Exception to denote that the current budget is locked by someone else
    """
    pass

class BudgetInvalidError(BudgetError):
    """
    Exception to denote that the database file contains an error or otherwise
    invalid data
    """
    pass
