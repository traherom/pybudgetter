import random
import string
import queue
import traceback
from pprint import pprint

from PyQt5.QtCore import Qt, QThread, pyqtSignal, pyqtSlot, QObject, QFile
from PyQt5.QtSql import QSqlQuery, QSqlDatabase

from ..resources_rc import *
from . import dbmodels, errors
from .helpers import *
from .errors import BudgetError, BudgetNoSuchRowError, BudgetLockedError, BudgetNotOpenError
from .scanner import scanner

class BudgetManager(QObject):
    """
    Core budget database management class. Handles connection to underlying database.
    """
    """Version of database this manager works with. Increasing integers only"""
    VERSION = 1

    # Signals
    activity_start = pyqtSignal(int)
    activity_progress = pyqtSignal(int, str)
    activity_end = pyqtSignal()
    activity_none = pyqtSignal()

    opened = pyqtSignal()
    closed = pyqtSignal()
    logins_changed = pyqtSignal()
    settings_changed = pyqtSignal()

    def __init__(self):
        super(BudgetManager, self).__init__()

        self.db = QSqlDatabase.addDatabase('QSQLITE')
        self.__scanner = None

        # Ready background processor
        self.bg_thread = QThread()
        self.processor = BudgetBackgroundThread()
        self.processor.lock()

        self.processor.moveToThread(self.bg_thread)
        self.bg_thread.started.connect(self.processor.processing_loop)
        self.processor.finished.connect(self.bg_thread.quit)
        self.processor.finished.connect(self.processor.deleteLater)
        self.bg_thread.finished.connect(self.bg_thread.deleteLater)

        self.processor.queue_empty.connect(self.activity_none.emit)

        self.bg_thread.start()

        self.opened.connect(self.__on_opened)
        self.closed.connect(self.__on_closed)

        self.opened.connect(self.__emit_all_internal_signals)
        self.closed.connect(self.__emit_all_internal_signals)

        # Create a semi-unique ID so we can easily tell if we are the particular instance that
        # locked the current budget
        self.__lock_key = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(20))

        self.__accounts = accounts.AccountsContainer(self)
        self.__categories = categories.CategoriesContainer(self)
        self.__transactions = transactions.TransactionsContainer(self)
        self.__rules = rules.RulesContainer(self)

    #def __del__(self):
    #    """Clean up. Primarily try to close the database nicely with all processing complete"""
    #    self.close()
    #    self.processor.stop()
    #    self.bg_thread.wait(3000)

    #####################
    # Signal chaining and internal work
    @pyqtSlot()
    def __emit_all_internal_signals(self):
        """Emits all Budget signals that aren't 'whole budget' things, like
        opened() and closed()"""
        print('Emitting all budget signals')
        self.logins_changed.emit()

    def perform_background_task(self, method, *args):
        """Takes the given method and runs it in the background thread"""
        self.processor.queue_task(method, *args)

    @pyqtSlot()
    def __on_opened(self):
        """Allow background processing requests again"""
        self.processor.unlock()

    @pyqtSlot()
    def __on_closed(self):
        """Prevent background processing requests"""
        self.processor.lock()

    #####################
    # Database basics
    def is_open(self):
        """Returns true if there is an open budget"""
        return self.db.isOpen()

    def lock(self):
        """Marks the current database as locked. By default, BudgetManager
        will refused to open a locked database"""
        try:
            v = self.get_setting('__lock')
            if v and v != self.__lock_key:
                # It's locked by someone else
                raise BudgetLockedError('The budget is already opened by someone else')
        except BudgetNoSuchRowError:
            self.set_setting('__lock', self.__lock_key)

    def unlock(self):
        """Unlocks the current database. No affect if there is no database."""
        if self.is_open():
            self.set_setting('__lock', '')

    def vacuum(self):
        """Runs VACUUM on the current budget"""
        query = self.query()
        if not query.prepare('VACUUM'):
            raise BudgetError('Unable to vacuum database and reclaim space')
        if not query.exec_():
            raise BudgetError('Unable to vacuum database and reclaim space')

    def cleanup(self):
        """Ensures the budget contains only 'relevant' data, stuff that is still
        actually referenced and accessible. A vacuum is performed afterward."""

        self.vacuum()

    def open(self, db_path, override_lock=False):
        """Opens a new budget database, closing any current one """
        if self.is_open():
            self.close()

        # Does it already exist? If not, create a blank DB
        if not QFile(db_path).exists():
            self.create(db_path)
            return

        # Prevent background processing until initialized
        self.processor.lock()

        try:
            self.db.setDatabaseName(db_path)
            if not self.db.open():
                raise BudgetError('Unable to open database', self.db.lastError())

            # Enable foreign keys
            q = self.query()
            if not q.prepare('PRAGMA foreign_keys=1;'):
                print('Unable to enable foreign key support')
            if not q.exec_():
                print('Unable to enable foreign key support')

            self.lock()
        except BudgetLockedError:
            if override_lock:
                # Force unlock it
                self.unlock()
            else:
                # Let the caller deal with it
                raise

        # Check version and automatically upgrade if needed
        try:
            self.check_version()
        except errors.BudgetVersionError as e:
            print(e)
            print('Attempting automatic upgrade')
            self.upgrade_database()

        # Every few opens, cleanup the database
        try:
            count = int(self.get_setting('__open_count'))
            self.set_setting('__open_count', count+1)

            if count % 10 == 0:
                print('Budget opened {} times, automatically vacuuming'.format(count))
                self.cleanup()
        except BudgetNoSuchRowError:
            self.set_setting('__open_count', 1)

        self.opened.emit()

    def close(self):
        """Closes any existing database. Does nothing if no database exists"""
        # Now close
        if self.is_open():
            # Ensure the background processor is done working on this database
            self.processor.lock()
            self.processor.wait_empty()

            self.closed.emit()
            self.unlock()
            self.db.close()

    def create(self, db_path):
        """Creates an empty budget database at the given path"""
        if self.is_open():
            self.close()

        # Refuse to touch an existing file
        if QFile(db_path).exists():
            raise BudgetError('File already exists, unable to create budget database')

        # Create database
        self.db.setDatabaseName(db_path)
        self.db.open()
        self.run_script(':budget.schema')
        self.set_setting('version', self.VERSION)
        self.db.close()

        # Open the new db the normal way
        self.open(db_path)

    def run_script(self, path):
        """Runs a script, given as a file name, against the database"""
        # Get the schema before we do anything else. If we're going to break, might as
        # well die before we even touch the new DB
        f = QFile(path)
        if not f.open(QFile.ReadOnly):
            raise FileNotFoundError('Unable to open script "{}"'.format(path), f.errorString())

        script = f.readAll()
        if not script:
            raise BudgetError('Script "{}" appears to be empty'.format(path), f.errorString())

        # Massage into the right format
        script = bytes(script).decode('ascii')

        try:
            self.start_transaction()

            # Execute each script statement one at a time
            query = self.query()
            for q in script.split(';'):
                # Skip newlines and comments
                q = q.strip()
                print('script: {}'.format(q))
                if not q or q.startswith('--'):
                    continue

                if not query.exec_(q):
                    # Failed to create database
                    raise BudgetError('Failed to run script completely (changes rolled back), failed on "{}"'.format(q),
                                      query.lastError())

            self.commit()
        except:
            self.rollback()
            raise

    @property
    def scanner(self):
        """Returns a scanner associated with this budget. All calls to this
        function return the same scanner instance"""
        if self.__scanner is None:
            self.__scanner = scanner.OnlineTransactionScanner(self)

        return self.__scanner

    def query(self):
        """Returns an open query"""
        if self.is_open():
            return QSqlQuery(self.db)
        else:
            raise BudgetNotOpenError('Unable to create new query, budget not open')

    def start_transaction(self):
        """Starts a new transaction"""
        if self.is_open():
            if not self.db.transaction():
                raise BudgetError('Unable to start transaction', self.db.lastError())

            print('Started transaction')
        else:
            raise BudgetNotOpenError('Unable to start transaction, budget not open')

    def commit(self):
        """Commits any existing transaction"""
        if self.is_open():
            if not self.db.commit():
                raise BudgetError('Unable to commit transaction', self.db.lastError())

            print('Committed transaction')
        else:
            raise BudgetNotOpenError('Unable to commit transaction, budget not open')

    def rollback(self):
        """Rollsback any existing transaction"""
        if self.is_open():
            if not self.db.rollback():
                raise BudgetError('Unable to rollback transaction', self.db.lastError())

            print('Rolled back transaction')
        else:
            raise BudgetNotOpenError('Unable to rollback transaction, budget not open')

    def check_version(self):
        """Checks if the budget database version is not current"""
        try:
            curr = int(self.get_setting('version'))
            if curr < self.VERSION:
                raise errors.BudgetVersionError('Budget database is version {} but expected version is {}'.format(curr, self.VERSION))
        except BudgetNoSuchRowError:
            raise errors.BudgetVersionError('Unable to determine budget version')

    def upgrade_database(self):
        """Upgrades the database to the newest version using the upgrade scripts successively"""
        prev = -2
        curr = -1
        while True:
            try:
                curr = int(self.get_setting('version'))
            except BudgetNoSuchRowError:
                curr = 0

            if curr == self.VERSION:
                break
            elif curr > self.VERSION:
                raise errors.BudgetVersionError('Database version ({}) is ahead of Budgetter version ({})'.format(curr, self.VERSION))

            # Watch for broken scripts
            if curr == prev:
                raise errors.BudgetVersionError('Database upgrade failed, stuck at version {}'.format(curr))
            prev = curr

            # Get script to go from curr to curr+1
            self.run_script(':upgrades/{}to{}.sql'.format(curr, curr+1))

        # If we haven't made it the correct version raise an exception
        self.check_version()

    #########################
    # Generic setting storage
    def set_setting(self, key, value):
        """Sets or creates a setting with the given value"""
        query = self.query()
        if not query.prepare("""INSERT OR REPLACE INTO settings (key, value) VALUES (?, ?)"""):
            raise BudgetError('Unable to change setting', query.lastError())

        query.addBindValue(key)
        query.addBindValue(value)

        if not query.exec_():
            raise BudgetError('Unable to change setting', query.lastError())

        self.settings_changed.emit()

    def get_setting(self, key):
        """Returns the value for the given setting. Throws an error if
        the setting cannot be found"""
        query = self.query()
        if not query.prepare("""SELECT value FROM settings WHERE key=?"""):
            raise BudgetError('Unable to get setting', query.lastError())

        query.addBindValue(key)

        if not query.exec_():
            raise BudgetError('Unable to get setting', query.lastError())

        if not query.next():
            raise BudgetNoSuchRowError('Unable to get setting', 'Setting "{}" not found'.format(key))

        return query.value(0)

    #########################
    # Logins
    def create_login(self, plugin):
        """Creates a login in the database and returns the new ID"""
        query = self.query()
        if not query.prepare('INSERT INTO logins (plugin) VALUES (?)'):
            raise BudgetError('Unable to create new login', query.lastError())

        query.addBindValue(plugin)

        if not query.exec_():
            raise BudgetError('Unable to create new login', query.lastError())

        self.logins_changed.emit()
        return query.lastInsertId()

    def remove_login(self, login_id):
        """Removes the given login from the database, including all accounts
        and transactions associated with it"""
        query = self.query()
        if not query.prepare('DELETE FROM logins WHERE id=?'):
            raise BudgetError('Unable to remove login', query.lastError())

        query.addBindValue(login_id)

        if not query.exec_():
            raise BudgetError('Unable to remove login', query.lastError())

        self.logins_changed.emit()

    def reset_login(self, login_id):
        """Resets the given login, removing all accounts
        and transactions associated with it"""
        query = self.query()
        if not query.prepare('DELETE FROM accounts WHERE login_id=?'):
            raise BudgetError('Unable to reset login', query.lastError())

        query.addBindValue(login_id)

        if not query.exec_():
            raise BudgetError('Unable to reset login', query.lastError())

    @property
    def logins(self):
        """
        Returns all logins in the database
        """
        pass

    def get_all_login_ids(self):
        """
        Returns all login IDs
        """
        query = self.query()
        if not query.prepare('SELECT id FROM logins'):
            raise BudgetError('Unable to get login IDs', query.lastError())

        if not query.exec_():
            raise BudgetError('Unable to get login IDs', query.lastError())

        ids = list()
        while query.next():
            ids.append(query.value(0))

        return ids

    def get_all_login_settings(self, login_id):
        """Returns a list of all settings for a given login """
        query = self.query()
        if not query.prepare('SELECT id FROM login_settings WHERE login_id=? ORDER BY id ASC'):
            raise BudgetError('Unable to get login settings', query.lastError())

        query.addBindValue(login_id)

        if not query.exec_():
            raise BudgetError('Unable to get login settings', query.lastError())

        settings = list()
        while query.next():
            settings.append(helpers.BudgetLoginSetting(self, query.value(0)))

        return settings

    def get_needed_login_settings(self, login_id):
        """Returns a list of settings that are marked as 'needed,' i.e. a user
        response is needed."""
        query = self.query()
        if not query.prepare('SELECT id FROM login_settings WHERE login_id=? AND needed=1 ORDER BY id ASC'):
            raise BudgetError('Unable to get login settings', query.lastError())

        query.addBindValue(login_id)

        if not query.exec_():
            raise BudgetError('Unable to get login settings', query.lastError())

        settings = list()
        while query.next():
            settings.append(helpers.BudgetLoginSetting(self, query.value(0)))

        return settings

    def get_login_plugin(self, login_id):
        """Returns the name of the plugin this login uses"""
        query = self.query()
        if not query.prepare('SELECT plugin FROM logins WHERE id=?'):
            raise BudgetError('Unable to get login plugin name', query.lastError())

        query.addBindValue(login_id)

        if not query.exec_():
            raise BudgetError('Unable to get login plugin name', query.lastError())

        if not query.next():
            raise BudgetNoSuchRowError('Unable to get login plugin name', 'Login not found')

        return query.value(0)

    def create_login_setting(self, login_id, key, value=''):
        """Creates a new login setting (if it already exists, returns the current setting)"""
        try:
            # Try to get it
            return self.get_login_setting(login_id=login_id, key=key)
        except BudgetError:
            # Create
            query = self.query()
            if not query.prepare('INSERT INTO login_settings (login_id, key, value) VALUES (?, ?, ?)'):
                raise BudgetError('Unable to create login setting', query.lastError())

            query.addBindValue(login_id)
            query.addBindValue(key)
            query.addBindValue(value)

            if not query.exec_():
                raise BudgetError('Unable to create login setting', query.lastError())

            sid = query.lastInsertId()
            self.logins_changed.emit()
            return self.get_login_setting(setting_id=sid)

    def get_login_setting(self, setting_id=None, login_id=None, key=None):
        """Returns a BudgetLoginSetting setting corresponding to the given login/key pair """
        return helpers.BudgetLoginSetting(self, setting_id=setting_id, login_id=login_id, key=key)

    ###############################
    # Accounts
    # Accounts must be associated with a login
    @property
    def accounts(self):
        return self.__accounts

    ###############################
    # Categories
    @property
    def categories(self):
        return self.__categories

    ###############################
    # Transactions
    @property
    def transactions(self):
        return self.__transactions

    ###############################
    # Rules
    @property
    def rules(self):
        return self.__rules

    ###############################
    # Factories for various models
    def create_login_model(self):
        """
        Creates a new instance of LoginModel
        """
        return dbmodels.LoginModel(self)

    def create_login_settings_model(self):
        """
        Creates a new instance of LoginSettingsModel
        """
        return dbmodels.LoginSettingsModel(self)

    def create_accounts_model(self):
        """Creates a new instance of AccountsListModel"""
        return accounts.AccountsModel(self)

    def create_transactions_model(self):
        """Creates a new instance of TransactionsModel"""
        return transactions.TransactionsModel(self)

    def create_category_model(self):
        """Creates a new instance of CategoryModel"""
        return categories.CategoryModel(self)

class BudgetBackgroundThread(QObject):
    queue_empty = pyqtSignal()
    task_started = pyqtSignal()
    task_done = pyqtSignal()
    finished = pyqtSignal()

    def __init__(self):
        super(BudgetBackgroundThread, self).__init__()
        self.queue = queue.Queue()
        self.run = False
        self.locked = False

    def queue_task(self, method, *args):
        """Pushes the given method onto the processing queue"""
        if not self.locked:
            self.queue.put([method, args])
        else:
            print('Ignoring request to perform {} as the background queue is locked'.format(method.__name__))

    def lock(self):
        """Prevents the processor from  accepting new tasks. NOT ATOMIC. Only
        stops new tasks in the future at some point, not guaranteed immediately."""
        print('Locked background queue')
        self.locked = True

    def unlock(self):
        """Allows the processor to resume accepting tasks. NOT ATOMIC. Only
        allows new tasks in the future at some point, not guaranteed immediately."""
        print('Unlocked background queue')
        self.locked = False

    def clear(self):
        """Clears all pending actions from the queue"""
        try:
            while True:
                self.queue.get(False)
                self.queue.task_done()
        except queue.Empty:
            print('Background processing queue emptied')
            self.queue_empty.emit()

    def wait_empty(self):
        """Waits until the queue is empty and returns"""
        self.queue.join()

    def stop(self):
        """Instructs the loop to terminate"""
        self.run = False

    def processing_loop(self):
        self.run = True
        while self.run:
            try:
                meth = self.queue.get(True, 2)
                self.task_started.emit()

                try:
                    print('Running {}'.format(meth[0].__name__))
                    meth[0](*(meth[1]))
                except BaseException as e:
                    print('Background task {} failed with an exception:'.format(meth[0].__name__))
                    traceback.print_tb(e.__traceback__)
                    print(e)
                    print('Background exception will continue with new tasks')

                self.queue.task_done()
                self.task_done.emit()

                # Notify if we have emptied the queue here rather than in the exception
                # to prevent spamming the empty signal
                if self.queue.empty():
                    self.queue_empty.emit()
            except queue.Empty:
                # Do nothing, just need to loop to check if we are supposed to die
                pass

        print('Stopped budget background thread')
        self.finished.emit()
