import importlib
import collections

from PyQt5.QtCore import *

from .. import filters, errors
from . import base

class RuleItem(base.CachingItemHelper):
    def __init__(self, budget, id=None):
        super(RuleItem, self).__init__(budget, budget.rules, id)

        self.__filters = []

        # If not an empty string, indicates there is a problem with this rule
        # and it will not perform any actions on transactions. Should be set
        # to a string describing problem(s) with the rule
        self.__is_broken = ''

        if self.__is_broken:
            print('Error loading rule {}: {}'.format(self.__id, self.__is_broken))

    @property
    def cached_attributes(self):
        return ('name', 'category_id')

    def _bind_to_budget(self):
        super(RuleItem, self)._bind_to_budget()

        # A category change could result in the name of the category we assign
        # to changing as well
        self.budget.categories.changed.connect(self.on_category_change)

    def _unbind_to_budget(self):
        if super(RuleItem, self)._unbind_to_budget():
            self.budget.categories.changed.disconnect(self.on_category_change)
            return True
        else:
            return False

    @pyqtSlot(object)
    def on_category_change(self, cat):
        if self.category_id == cat.id:
            self.budget.rules.changed.emit(self)

    def _save_existing(self):
        """Overwrites an existing rule"""
        # Top level rule
        print('Saving existing rule (rule id {})'.format(self.id))
        query = self.budget.query()
        if not query.prepare("""UPDATE rules
                                    SET name=?, category_id=?
                                    WHERE id=?"""):
            raise errors.BudgetError('Unable to save rule', query.lastError())

        query.addBindValue(self.name)
        query.addBindValue(self.category_id)
        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to save rule', query.lastError())

        # Remove all existing filter data
        query = self.budget.query()
        if not query.prepare('DELETE FROM rule_filters WHERE rule_id=?'):
            raise errors.BudgetError('Unable to delete existing filters', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to delete existing filters', query.lastError())

        # Insert filters for rule
        for f in self.filters:
            f.save(self.id)

    def _save_new(self):
        """Creates a new rule"""
        # Top level rule
        print('Saving new rule (rule id {})'.format(self.id))
        query = self.budget.query()
        if not query.prepare("""INSERT INTO rules (name, category_id)
                                    VALUES (?, ?)"""):
            raise errors.BudgetError('Unable to create new rule', query.lastError())

        query.addBindValue(self.name)
        query.addBindValue(self.category_id)

        if not query.exec_():
            raise errors.BudgetError('Unable to create new rule', query.lastError())

        self.id = query.lastInsertId()

        # Insert filters for rule
        for f in self.filters:
            f.save(rule_id=self.id)

    def _load_from_db(self):
        # Get the general rule info
        query = self.budget.query()
        if not query.prepare('SELECT name, category_id FROM rules WHERE id=?'):
            raise errors.BudgetError('Unable to load rule from database', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to load rule from database', query.lastError())
        if not query.next():
            raise errors.errors.BudgetNoSuchRowError('Rule ID {} not found'.format(id))

        self.name = query.value(0)
        self.category_id = query.value(1)

        # Load all the classes that are part of this rule
        query = self.budget.query()
        if not query.prepare('SELECT id, filter_class FROM rule_filters WHERE rule_id=?'):
            raise errors.BudgetError('Unable to load rule filters from database', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to load rule filters from database', query.lastError())

        defaultmodule = importlib.import_module('pybudgetter.budget')

        self.__filters = []
        while query.next():
            try:
                full = query.value(1)

                all = full.split('.')
                clsname = all[-1]
                modulename = '.'.join(all[:-1])

                if modulename:
                    module = importlib.import_module(modulename)
                    c = getattr(module, clsname)
                else:
                    c = getattr(defaultmodule, clsname)

                self.filters.append(c.load(self.budget, query.value(0)))
            except AttributeError as e:
                # The filter class named could not be found
                self.__is_broken += 'Rule with filter "{}" could not be loaded: {}'.format(full, e)
            except KeyError as e:
                self.__is_broken += 'Setting "{}" for filter "{}" could not be loaded'.format(e, full)

    @property
    def filters(self):
        return self.__filters

    @filters.setter
    def filters(self, filters):
        if isinstance(filters, collections.MutableSequence):
            self.__filters = filters
        else:
            self.__filters = list(filters)

    @property
    def category(self):
        return self.budget.categories[self.category_id]

    def _delete_from_db(self):
        query = self.budget.query()
        if not query.prepare('''DELETE FROM rules WHERE id=?'''):
            raise errors.BudgetError('Unable to delete rule', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to delete rule', query.lastError())

    def filter_model(self, model, row):
        if self.__is_broken:
            return False

        for f in self.__filters:
            if not f.filter_model(model, row):
                return False
        return True

    def filter_transaction(self, trans):
        if self.__is_broken:
            return False

        for f in self.__filters:
            if not f.filter_transaction(trans):
                return False
        return True

    def apply(self, trans):
        if self.filter_transaction(trans):
            trans.category_id = self.category_id
            return True
        else:
            return False

    def rule_summary_str(self):
        if not self.__is_broken:
            all = ' AND '.join([str(f) for f in self.filters])
            if not all:
                all = 'anything'

            return 'If transaction {}'.format(all)
        else:
            return 'Rule broken, please edit: {}'.format(self.__is_broken)

    def __str__(self):
        return '{} then cat {}'.format(self.rule_summary_str(), self.category_id)

class RulesContainer(base.CachingItemContainer):
    """Presents a clean interface to work with the accounts in the current database"""
    def __init__(self, budget):
        super(RulesContainer, self).__init__(budget)

    def rebuild_cache(self):
        self.cache = {}

        if self.budget.is_open():
            query = self.budget.query()
            if not query.prepare('SELECT id FROM rules'):
                raise errors.BudgetError('Unable to get rule list from database', query.lastError())

            if not query.exec_():
                raise errors.BudgetError('Unable to get rule list from database', query.lastError())

            while query.next():
                self.cache[query.value(0)] = RuleItem.from_id(self.budget, query.value(0))

class RulesModel(base.BudgetDrivenModel):
    def __init__(self, budget):
        super(RulesModel, self).__init__(budget,
            budget.rules,
            ('id', 'name', 'desc', 'cat_id', 'cat_name'))

        # Headers
        self.setHeaderData(self.id_col, Qt.Horizontal, 'ID')
        self.setHeaderData(self.name_col, Qt.Horizontal, 'Name')
        self.setHeaderData(self.desc_col, Qt.Horizontal, 'Description')
        self.setHeaderData(self.cat_id_col, Qt.Horizontal, 'Category')
        self.setHeaderData(self.cat_name_col, Qt.Horizontal, 'Category')

    def complete_rule(self, index):
        return self.budget.rules[self.get_id_for_index(index)]

    def data(self, index, role=Qt.DisplayRole):
        col = index.column()
        rule = self.complete_rule(index)

        if role == Qt.DisplayRole:
            if col == self.id_col:
                return rule.id
            elif col == self.name_col:
                return rule.name
            elif col == self.desc_col:
                return rule.rule_summary_str()
            elif col == self.cat_id_col:
                return rule.category_id
            elif col == self.cat_name_col:
                return rule.category.name

        return QVariant()
