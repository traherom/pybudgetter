from PyQt5.QtCore import QObject, QAbstractTableModel, QModelIndex, Qt, pyqtSlot, pyqtSignal

from .. import errors

class CachingItemHelper(QObject):
    """Handles lazy loading from the data Children should connect
    appropriate signals to budget to ensure the cache is cleared"""
    def __init__(self, budget, container, id=None):
        super(CachingItemHelper, self).__init__()

        # Need to bootstrap the object with some hand-set values
        self.is_valid = True
        self.container = container
        self.loading = False
        self.cache = {}
        self.budget = budget
        self.bound = False

        self.__id = None
        self.id = id

    def __del__(self):
        try:
            self._unbind_to_budget()
        except TypeError:
            # Deletion gets messy and for unknown reasons we try to unbind when
            # we aren't really. Would like to figure out WHY, but there's no
            # harm in swallowing the error
            pass

    @property
    def id(self):
        return self.__id

    @id.setter
    def id(self, val):
        if self.id is None:
            self.__id = val
        else:
            raise ValueError('ID already exists for account')

        if self.id is not None:
            self._bind_to_budget()

    @classmethod
    def from_id(cls, budget, id):
        return cls(budget, id)

    @classmethod
    def from_values(cls, budget, **values):
        acc = cls(budget)
        required = acc.cached_attributes
        if set(values.keys()) != set(required):
            raise AttributeError('Not all required attributes set for class. Required: {}'.format(
                required))

        for k, v in values.items():
            setattr(acc, k, v)

        return acc

    def _bind_to_budget(self):
        """Connects to signal(s) on budget to clear cache when appropriate.
        Should be overridden by children."""
        if not self.bound:
            # Basic changes to ourselves
            self.container.changed.connect(self.on_changed)
            self.container.removed.connect(self.on_removed)

            self.bound = True

    def _unbind_to_budget(self):
        """Disconnects to signal(s) on budget. Should be overridden by children.
        Returns True if actually unbinds"""
        try:
            if self.bound:
                self.container.changed.disconnect(self.on_changed)
                self.container.removed.disconnect(self.on_removed)
                self.bound = False
                return True
            else:
                return False
        except TypeError:
            return False

    def _delete_from_db(self):
        """Performs the actual deletion from the database"""
        raise NotImplementedError('Children must implement _delete_from_db()')

    def remove(self):
        """Removes this item from the data"""
        self.container.remove(self)

    def load(self):
        """Loads the item from the data. Values must be stored in self.cache
        dictionary to work properly"""
        if not self.is_valid:
            raise errors.BudgetError('Unable to load previously deleted item')

        self._load_from_db()

    def _load_from_db(self):
        """Performs actual database load"""
        raise NotImplementedError('Children must implement load()')

    def save(self):
        """Saves the item and ensure it is the proper category"""
        if not self.is_valid:
            raise errors.BudgetError('Unable to save previously deleted item')

        self.save_no_append()
        self.container.append(self)

    def save_no_append(self):
        """Saves the item to the database. Raises a ValueError if a value has
        not been set properly yet."""
        if not self.is_valid:
            raise errors.BudgetError('Unable to save previously deleted item')

        if self.id is None:
            self._save_new()
            #return self.container.append(self)
        else:
            self._save_existing()
            self.container.changed.emit(self)

    def _save_new(self):
        """Saves a new item to the database, setting ID properly at the end"""
        raise NotImplementedError('Children must implement _save_new()')

    def _save_existing(self):
        """Saves an existing item to the database"""
        raise NotImplementedError('Children must implement _save_existing()')

    def _emit_changed(self):
        """Called to emit the correct changed signal. Must be implemented by children"""
        self.container.changed.emit(self)

    def clear_cache(self):
        self.cache = {}

    def check_cache(self):
        if not self.loading and not self.cache:
            if self.id is not None:
                try:
                    self.loading = True
                    self.load()
                finally:
                    self.loading = False

    @property
    def cached_attributes(self):
        """Returns a list of attribute names that are cached for the sake of the
        child. Children must override this function to return an iterable of
        strings giving the names of attributes they use."""
        raise NotImplementedError('Children must implement cached_attributes')

    @pyqtSlot(object)
    def on_changed(self, item):
        if self is None:
            return

        # Only clear data if we have a way to load it back in
        if item.id == self.id and self.is_valid:
            self.clear_cache()

    @pyqtSlot(object)
    def on_removed(self, item):
        if self is None:
            return

        if item == self or item.id == self.id:
            self.is_valid = False

    def __getattr__(self, name):
        try:
            self.check_cache()
            return self.cache[name]
        except KeyError:
            raise AttributeError('Attribute "{}" not found'.format(name))

    def __setattr__(self, name, value):
        #print('setting {} to {}'.format(name, value))
        if name in self.cached_attributes:
            # Load other values if needed, otherwise setting this cached attribute
            # will make check_cache() believe everything is loaded
            self.check_cache()
            self.cache[name] = value
            #self._emit_changed()
        else:
            super(CachingItemHelper, self).__setattr__(name, value)

class CachingItemContainer(QObject):
    """Presents a clean interface to work with a list of items
    in the current data IE, accounts, logins, transactions, etc"""

    reset = pyqtSignal()
    added = pyqtSignal(CachingItemHelper)
    removing = pyqtSignal(CachingItemHelper)
    removed = pyqtSignal(CachingItemHelper)
    changed = pyqtSignal(CachingItemHelper)

    def __init__(self, budget):
        super(CachingItemContainer, self).__init__()

        self.budget = budget
        self.budget.opened.connect(self.on_reloaded)
        self.budget.closed.connect(self.on_reloaded)
        self.bound = True

        self._bind_to_budget()
        self.reload()

    def _bind_to_budget(self):
        """Binds budget signals to this container. Should be overridden by children"""
        pass

    def _unbind_to_budget(self):
        return self.bound

    def rebuild_cache(self):
        """Caches all items in the database into memory"""
        raise NotImplementedError('Children must implement rebuild_cache()')

    def append(self, item):
        """Adds a new item to the database. Returns True if it succeeds, False
        if the item already exists in the dictionary."""
        # Item must already have an ID to be appended
        if item.id is None:
            item.save_no_append()

        if item.id not in self.cache:
            # Add to list and save
            self.cache[item.id] = item
            self.added.emit(item)
            return True
        else:
            # Ignore
            return False

    def append_list(self, items):
        """Appends ALL the given items. Sends a reset signal at the end, rather
        than individual added signals for each one."""
        for item in items:
            if item.id is None:
                item.save_no_append()

            #if item.id not in self.cache:
            #    self.cache[item.id] = item

        self.reload()

    def remove(self, item):
        """Tells the given item to remove itself from the database. Returns True
        if the item is successfully removed, False if it does not exist"""
        try:
            # Ensure we have the actual item
            if type(item) == int:
                item = self[item]
        except KeyError:
            # Item already doesn't exist
            return False

        # Drop it from our list if it exists
        if item.id in self.cache:
            try:
                # Remove the item from the database first, that way if it fails
                # we won't remove it from our own list
                item._delete_from_db()
                item._unbind_to_budget()
                item.is_valid = False

                self.removing.emit(item)
                self.cache.pop(item.id, None)
                self.removed.emit(item)

                return True
            except errors.BudgetError as e:
                print(e)
                raise
        else:
            return False

    @pyqtSlot()
    def on_reloaded(self):
        self.reload()

    def __len__(self):
        return len(self.cache)

    def __getitem__(self, id):
        return self.cache[id]

    def __iter__(self):
        return ItemContainerIterator(self)

    def __contains__(self, item):
        return item in self.cache

    def __bool__(self):
        return len(self) > 0

    def keys(self):
        """Returns a list of all the IDs in this list"""
        return self.cache.keys()

    def find(self, **terms):
        """Finds the first item with the given terms matching. Returns
        None if not found."""
        for item in self:
            #print('checking {}'.format(item.name))
            matched = True
            for name, val in terms.items():
                #print('checking {} ({} == {})?'.format(name, getattr(item, name), val))
                if getattr(item, name) != val:
                    matched = False
                    break

            if matched:
                return item

        return None

    def key_for_index(self, index):
        """Returns the key (id) for the given index (count into dict)"""
        return list(self.cache.keys())[index]

    def item_for_index(self, index):
        """Returns the item at the given index (count into dict)"""
        return self[self.key_for_index(index)]

    def reload(self):
        """Rebuilds the ID cache and signals the reset"""
        self.rebuild_cache()
        self.reset.emit()

class ItemContainerIterator:
    def __init__(self, container):
        self.__container = container
        self.__keys = list(container.cache.keys())
        self.__curr = 0

    def __iter__(self):
        return self

    def __next__(self):
        try:
            item = self.__container[self.__keys[self.__curr]]
            self.__curr += 1
            return item
        except IndexError:
            raise StopIteration()

class BudgetDrivenModel(QAbstractTableModel):
    def __init__(self, budget, container, columns):
        super(BudgetDrivenModel, self).__init__()

        self.budget = budget
        self.container = container
        self.__horz_headers = {}

        if 'id' not in columns:
            raise ValueError('BudgetDrivenModel children must include an "id" column')

        self.__col_order = dict(zip(['{}_col'.format(n) for n in columns],
                                    range(len(columns))))

        self.id_cache = None
        self._bind_to_budget()
        self.reinit_model()

    def _bind_to_budget(self):
        """Binds to appropriate budget signals to update model. Must be overridden
        by children."""
        self.container.reset.connect(self.on_reset)
        self.container.added.connect(self.on_add)
        self.container.removed.connect(self.on_removed)
        #self.container.removing.connect(self.on_removing)
        self.container.changed.connect(self.on_change)

    @property
    def ids(self):
        """Returns a list of all IDs that are part of this model"""
        if self.id_cache is None:
            self.rebuild_id_cache()

        return self.id_cache

    def _get_ids(self):
        """Returns a list of all IDs that are part of this model. MUST return
        IDs in a consistent order. Sorting changes, if implemented, should not
        affect this function."""
        return list(self.container.keys())

    def rebuild_id_cache(self):
        self.beginResetModel()
        self.reinit_model()
        self.id_cache = self._get_ids()
        self.endResetModel()

    def __getattr__(self, name):
        try:
            return self.__col_order[name]
        except KeyError:
            raise AttributeError('object has no attribute \'{}\''.format(name))

    def columnCount(self, parent=QModelIndex()):
        return len(self.__col_order)

    def rowCount(self, parent=QModelIndex()):
        if self.id_cache is not None:
            return len(self.id_cache)
        else:
            return 0

    def setHeaderData(self, section, orientation, value, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            self.__horz_headers[section] = value
        else:
            return super(BudgetDrivenModel, self).setHeaderData(section, orientation, value, role)

    def headerData(self, section, orientation, role=Qt.DisplayRole):
        if orientation == Qt.Horizontal and role == Qt.DisplayRole:
            try:
                return self.__horz_headers[section]
            except KeyError:
                return section
        else:
            return super(BudgetDrivenModel, self).headerData(section, orientation, role)

    def get_id_for_index(self, index):
        """Returns the id for the given index/row. If an index
        is given, the row is extracted from it"""
        return self.ids[index.row()]

    def get_row_for_id(self, id):
        """Returns the index for the given ID. Returns None if this ID does
        not exist in model."""
        try:
            return self.ids.index(id)
        except ValueError:
            return None

    def reinit_model(self):
        """Called when the ID cache is being rebuilt. May be overridden by children
        to clear internal data if needed"""
        pass

    @pyqtSlot(CachingItemHelper)
    def on_change(self, item):
        """Handles an item changing values, need to have view redisplay that row"""
        row = self.get_row_for_id(item.id)
        if row is not None:
            topleft = self.index(row, 0)
            bottomright = self.index(topleft.row(), self.columnCount()-1)
            self.dataChanged.emit(topleft, bottomright)

    @pyqtSlot(CachingItemHelper)
    def on_add(self, item):
        """Handles an item being added to the table"""
        self.rebuild_id_cache()
        row = self.get_row_for_id(item.id)

        if row is not None:
            self.beginInsertRows(QModelIndex(), row, row)
            self.endInsertRows()

    @pyqtSlot(CachingItemHelper)
    def on_removing(self, item):
        """Handles an item being removed from the table"""
        row = self.get_row_for_id(item.id)
        if row is not None:
            self.beginRemoveRows(QModelIndex(), row, row)
            self.rebuild_id_cache()
            self.endRemoveRows()

    @pyqtSlot(CachingItemHelper)
    def on_removed(self, item):
        # Before we blow away our cached ID list, find the row this item was on
        row = self.get_row_for_id(item.id)
        if row is not None:
            self.beginRemoveRows(QModelIndex(), row, row)
            self.rebuild_id_cache()
            self.endRemoveRows()
        else:
            # Unable to determine what is changing
            self.beginResetModel()
            self.rebuild_id_cache()
            self.endResetModel()

    @pyqtSlot()
    def on_reset(self):
        self.beginResetModel()
        self.rebuild_id_cache()
        self.endResetModel()
