from pprint import pprint

from PyQt5.QtCore import Qt, QDate, pyqtSlot, QVariant

from . import base
from .. import utilities, errors

class TransactionItem(base.CachingItemHelper):
    def __init__(self, budget, id=None):
        super(TransactionItem, self).__init__(budget, budget.transactions, id)

    @property
    def cached_attributes(self):
        return ('account_id', 'date', 'description', 'amount', 'category_id')

    def _bind_to_budget(self):
        if super(TransactionItem, self)._bind_to_budget():
            # Transaction category name/even having one could change if the category
            # it is assigned to changed
            self.budget.categories.changed.connect(self.on_category_change)
            self.budget.categories.removing.connect(self.on_category_removing)

            # Account change could be a rename or a removal of the transaction
            self.budget.accounts.changed.connect(self.on_account_change)
            self.budget.accounts.removing.connect(self.on_account_removing)
            return True
        else:
            return False

    def _unbind_to_budget(self):
        if super(TransactionItem, self)._unbind_to_budget():
            self.budget.categories.changed.disconnect(self.on_category_change)
            self.budget.categories.removing.disconnect(self.on_category_removing)

            self.budget.accounts.changed.disconnect(self.on_account_change)
            self.budget.accounts.removing.disconnect(self.on_account_removing)
            return True
        else:
            return False

    @pyqtSlot(object)
    def on_category_change(self, cat):
        if self.category_id == cat.id:
            self._emit_changed()

    @pyqtSlot(object)
    def on_category_removing(self, cat):
        if self.category_id == cat.id:
            self.category_id = None
            self._emit_changed()

    @pyqtSlot(object)
    def on_account_change(self, acc):
        if self.account_id == acc.id:
            self._emit_changed()

    @pyqtSlot(object)
    def on_account_removing(self, acc):
        if self.account_id == acc.id:
            self.is_valid = False
            # Don't need to remove ourselves, the container will reload
            #self.budget.transactions.remove(self)

    def _save_new(self):
        query = self.budget.query()
        if not query.prepare('''INSERT INTO transactions (account_id, date,
                                                          description, amount,
                                                          category_id)
                                    VALUES (?, ?, ?, ?, ?)'''):
            raise errors.BudgetError('Unable to create transaction', query.lastError())

        query.addBindValue(self.account_id)
        query.addBindValue(self.date.isoformat(' '))
        query.addBindValue(self.description)
        query.addBindValue(float(self.amount))
        query.addBindValue(self.category_id)

        if not query.exec_():
            raise errors.BudgetError('Unable to create transaction', query.lastError())

        self.id = query.lastInsertId()

    def _save_existing(self):
        query = self.budget.query()
        if not query.prepare('''UPDATE transactions SET account_id=?,
                                                        date=?,
                                                        description=?,
                                                        amount=?,
                                                        category_id=?
                                    WHERE id=?'''):
            raise errors.BudgetError('Unable to update transaction', query.lastError())

        query.addBindValue(self.account_id)
        query.addBindValue(self.date.isoformat(' '))
        query.addBindValue(self.description)
        query.addBindValue(float(self.amount))
        query.addBindValue(self.category_id)
        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to update transaction', query.lastError())

    def _load_from_db(self):
        query = self.budget.query()
        if not query.prepare('''SELECT account_id, date, description, amount, category_id
                                    FROM transactions
                                    WHERE id=?'''):
            raise errors.BudgetError('Unable to load transaction', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to load transaction', query.lastError())
        if not query.next():
            raise errors.BudgetNoSuchRowError('Transaction ID {} not found'.format(self.id))

        self.account_id = query.value(0)
        self.date = utilities.parse_iso_date(query.value(1))
        self.description = query.value(2)
        self.amount = query.value(3)
        cat = query.value(4)
        if cat != '':
            self.category_id = cat
        else:
            self.category_id = None

    def _delete_from_db(self):
        query = self.budget.query()
        if not query.prepare('''DELETE FROM transactions WHERE id=?'''):
            raise errors.BudgetError('Unable to delete transaction', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to delete transaction', query.lastError())

    @property
    def account(self):
        """Returns the actual account this transaction is under (vs the id)"""
        return self.budget.accounts[self.account_id]

    @property
    def category(self):
        """Returns the actual category this transaction is under (vs the id)"""
        if self.category_id is not None:
            return self.budget.categories[self.category_id]
        else:
            return None

    def apply_rules(self):
        """Attempts to categorize this transaction based on the rules in the budget.
        Returns True if any changes are made, False otherwise."""
        for rule in self.budget.rules:
            if rule.apply(self):
                return True
        return False

class TransactionsContainer(base.CachingItemContainer):
    """Presents a clean interface to work with the transactions in the current database"""
    def __init__(self, budget):
        super(TransactionsContainer, self).__init__(budget)

    def _bind_to_budget(self):
        """Watch for account removals, which may remove a huge number of transactions
        at once."""
        self.budget.accounts.removed.connect(self.on_account_removed)

    @pyqtSlot(base.CachingItemHelper)
    def on_account_removed(self, acc):
        self.reload()

    def get_newest(self, account_id):
        """Returns the most recent transaction for a given account"""
        query = self.query()
        if not query.prepare("""SELECT id FROM transactions
                                    WHERE account_id=?
                                    ORDER BY date DESC LIMIT 1"""):
            raise errors.BudgetError('Unable to fetch transactions', query.lastError())

        query.addBindValue(account_id)

        if not query.exec_():
            raise errors.BudgetError('Unable to fetch transactions', query.lastError())

        if not query.next():
            raise errors.BudgetNoSuchRowError('Unable to fetch transactions', query.lastError())

        return self[query.value(0)]

    def income_used_total(self, from_date, to_date):
        """Determines which categories are positive (income) and get all transactions
        that are associated with those transactions, totalling them over the dates
        given."""
        if not self.budget.is_open():
            return 0.0

        query = self.budget.query()
        if not query.prepare("""SELECT sum(t.amount)
                                    FROM categories c
                                    JOIN transactions t ON t.category_id=c.id
                                        AND t.date BETWEEN ? AND ?
                                    JOIN accounts a ON t.account_id=a.id AND a.hidden=0
                                    WHERE c.amount>0;"""):
            raise errors.BudgetError('Unable to get used income total', query.lastError())

        query.addBindValue(from_date.toString(Qt.ISODate))
        query.addBindValue(to_date.toString(Qt.ISODate))

        if not query.exec_():
            raise errors.BudgetError('Unable to get used income total', query.lastError())

        if not query.next():
            raise errors.BudgetError('Unable to get used income total', query.lastError())

        val = query.value(0)
        if val:
            return val
        else:
            return 0.0

    def expenses_used_total(self, from_date, to_date):
        """Determines which categories are negative (expenses) and get all transactions
        that are associated with those transactions, totalling them over the dates
        given."""
        if not self.budget.is_open():
            return 0.0

        query = self.budget.query()
        if not query.prepare("""SELECT sum(t.amount)
                                    FROM categories c
                                    JOIN transactions t ON t.category_id=c.id
                                        AND t.date BETWEEN ? AND ?
                                    JOIN accounts a ON t.account_id=a.id AND a.hidden=0
                                    WHERE c.amount<0;"""):
            raise errors.BudgetError('Unable to get used expenses total', query.lastError())

        query.addBindValue(from_date.toString(Qt.ISODate))
        query.addBindValue(to_date.toString(Qt.ISODate))

        if not query.exec_():
            raise errors.BudgetError('Unable to get used expenses total', query.lastError())

        if not query.next():
            raise errors.BudgetError('Unable to get used expenses total', query.lastError())

        val = query.value(0)
        if val:
            return val
        else:
            return 0.0

    def find(self, **terms):
        """Do finds by querying the database, rather than straight scanning our
        lists."""
        sql = 'SELECT id FROM transactions WHERE {}'.format(
                ' AND '.join(['{}="{}"'.format(k, v) for k, v in terms.items()]))
        #print(sql)

        query = self.budget.query()
        if not query.prepare(sql):
            raise errors.BudgetError('Unable to search transactions', query.lastError())

        if not query.exec_():
            raise errors.BudgetError('Unable to search transactions', query.lastError())

        if query.next():
            return query.value(0)
        else:
            return None

    def __getitem__(self, id):
        if self.cache[id] is None:
            self.cache[id] = TransactionItem.from_id(self.budget, id)

        return self.cache[id]

    def rebuild_cache(self):
        self.cache = {}

        if self.budget.is_open():
            query = self.budget.query()
            if not query.prepare('SELECT id FROM transactions'):
                raise errors.BudgetError('Unable to get transaction list from database', query.lastError())

            if not query.exec_():
                raise errors.BudgetError('Unable to get transactions list from database', query.lastError())

            while query.next():
                # Rather than loading each transaction, just denote that given IDs exist
                self.cache[query.value(0)] = None

class TransactionsModel(base.BudgetDrivenModel):
    def __init__(self, budget):
        super(TransactionsModel, self).__init__(budget,
            budget.transactions,
            ('id', 'account_id', 'account_name', 'date',
             'desc', 'amount', 'cat', 'cat_desc'))

        # ID cache for just IDs that match our date range and aren't hidden
        self.__ids = []

        # By default only get within a month of today
        self.__to_date = QDate.currentDate().addMonths(1)
        self.__from_date = QDate.currentDate().addMonths(-1)

        # Headers
        self.setHeaderData(self.id_col, Qt.Horizontal, 'ID')
        self.setHeaderData(self.account_id_col, Qt.Horizontal, 'Account')
        self.setHeaderData(self.account_name_col, Qt.Horizontal, 'Account')
        self.setHeaderData(self.date_col, Qt.Horizontal, 'Date')
        self.setHeaderData(self.desc_col, Qt.Horizontal, 'Description')
        self.setHeaderData(self.amount_col, Qt.Horizontal, 'Amount')
        self.setHeaderData(self.cat_col, Qt.Horizontal, 'Category ID')
        self.setHeaderData(self.cat_desc_col, Qt.Horizontal, 'Category')

    def _get_ids(self):
        return self.__ids

    def set_to_date(self, date):
        self.__to_date = date
        self.rebuild_id_cache()

    def set_from_date(self, date):
        self.__from_date = date
        self.rebuild_id_cache()

    def data(self, index, role=Qt.DisplayRole):
        col = index.column()
        trans = self.budget.transactions[self.get_id_for_index(index)]

        if role == Qt.DisplayRole or role == Qt.EditRole:
            if col == self.id_col:
                return trans.id
            elif col == self.desc_col:
                return trans.description
            elif col == self.account_name_col:
                return trans.account.name
            elif col == self.date_col:
                return trans.date
            elif col == self.amount_col:
                return trans.amount
            elif col == self.cat_col:
                return trans.category_id
            elif col == self.cat_desc_col:
                if trans.category:
                    return trans.category.name
                else:
                    return ''

        return QVariant()

    @pyqtSlot(base.CachingItemHelper)
    def on_add(self, trans):
        """Only put this person in our ID list if they are actually in our date limits"""
        if self.__from_date <= trans.date <= self.__to_date:
            self.reinit_model()
            super(TransactionsModel, self).on_add(trans)

    def reinit_model(self):
        """Resets the model query"""
        if not self.budget.is_open():
            self.__ids = []
            return

        query = self.budget.query()
        if not query.prepare("""SELECT t.id
                                    FROM transactions t
                                    JOIN accounts a ON a.id=t.account_id
                                        AND a.hidden=0
                                    WHERE t.date BETWEEN ? AND ?
                                    ORDER BY t.date DESC"""):
            raise errors.BudgetError('Failed to create TransactionsModel', query.lastError())

        query.addBindValue(self.__from_date.toString(Qt.ISODate))
        query.addBindValue(self.__to_date.toString(Qt.ISODate))

        if not query.exec_():
            raise errors.BudgetError('Failed to create TransactionsModel', query.lastError())

        self.__ids = []
        while query.next():
            self.__ids.append(query.value(0))

