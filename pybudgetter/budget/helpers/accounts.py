from pprint import pprint

from PyQt5.QtCore import Qt, QVariant

from . import base
from .. import errors, importer

class AccountItem(base.CachingItemHelper):
    def __init__(self, budget, id=None):
        super(AccountItem, self).__init__(budget, budget.accounts, id)

    @property
    def cached_attributes(self):
        return ('name', 'hidden', 'account_number', 'routing_number')

    @property
    def visible(self):
        return not self.hidden

    @visible.setter
    def visible(self, is_visible):
        self.hidden = not is_visible

    def show(self, visible=True):
        """Shows/hides this account"""
        self.hidden = not visible

    def import_transactions(self, tfile):
        """Imports the given transaction file. File must be a file-like object"""
        importer.from_file(tfile, self)

    def _load_from_db(self):
        """Loads/reloads the account data from the database"""
        if not self.budget.is_open():
            return

        query = self.budget.query()
        if not query.prepare('''SELECT name, hidden, account_number, routing_number
                                    FROM accounts WHERE id=?'''):
            raise errors.BudgetError('Unable to load account from database', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to load account from database', query.lastError())
        if not query.next():
            raise errors.BudgetNoSuchRowError('Unable to account with ID {} not found'.format(self.id))

        self.name = query.value(0)
        self.hidden = query.value(1)
        self.account_number = str(query.value(2))
        self.routing_number = str(query.value(3))

    def _save_new(self):
        query = self.budget.query()
        if not query.prepare('''INSERT INTO accounts (name, hidden,
                                                      account_number, routing_number)
                                    VALUES (?, ?, ?, ?)'''):
            raise errors.BudgetError('Unable to create account', query.lastError())

        query.addBindValue(self.name)
        query.addBindValue(self.hidden)
        query.addBindValue(self.account_number)
        query.addBindValue(self.routing_number)

        if not query.exec_():
            raise errors.BudgetError('Unable to create account', query.lastError())

        self.id = query.lastInsertId()

    def _save_existing(self):
        query = self.budget.query()
        if not query.prepare('''UPDATE accounts SET name=?,
                                                    hidden=?,
                                                    account_number=?,
                                                    routing_number=?
                                    WHERE id=?'''):
            raise errors.BudgetError('Unable to update account', query.lastError())

        query.addBindValue(self.name)
        query.addBindValue(self.hidden)
        query.addBindValue(self.account_number)
        query.addBindValue(self.routing_number)
        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to update account', query.lastError())

    @property
    def transaction_ids(self):
        """Returns a list of the IDs of all the transactions belonging to this account"""
        query = self.budget.query()
        if not query.prepare('SELECT id FROM transactions WHERE account_id=?'):
            raise errors.BudgetError('Unable to find transaction IDs')

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to find transaction IDs')

        ids = list()
        while query.next():
            ids.append(query.value(0))

        return ids

    def clear_transactions(self):
        """Removes all transactions associated with this account"""
        ids = self.transaction_ids
        for id in ids:
            self.budget.transactions[id].remove()

    def _delete_from_db(self):
        query = self.budget.query()
        if not query.prepare('''DELETE FROM accounts WHERE id=?'''):
            raise errors.BudgetError('Unable to remove account', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to remove account', query.lastError())

    def __str__(self):
        return self.name

class AccountsContainer(base.CachingItemContainer):
    """Presents a clean interface to work with the accounts in the current database"""
    def __init__(self, budget):
        super(AccountsContainer, self).__init__(budget)

    def rebuild_cache(self):
        self.cache = {}

        if self.budget.is_open():
            query = self.budget.query()
            if not query.prepare('SELECT id FROM accounts'):
                raise errors.BudgetError('Unable to get account list from database', query.lastError())

            if not query.exec_():
                raise errors.BudgetError('Unable to get account list from database', query.lastError())

            while query.next():
                self.cache[query.value(0)] = AccountItem.from_id(self.budget, query.value(0))

class AccountsModel(base.BudgetDrivenModel):
    def __init__(self, budget):
        super(AccountsModel, self).__init__(budget,
            budget.accounts,
            ('id', 'name', 'is_hidden', 'account_number', 'routing_number'))

        # Headers
        self.setHeaderData(self.id_col, Qt.Horizontal, 'ID')
        self.setHeaderData(self.name_col, Qt.Horizontal, 'Name')
        self.setHeaderData(self.is_hidden_col, Qt.Horizontal, 'Shown')
        self.setHeaderData(self.account_number_col, Qt.Horizontal, 'Account #')
        self.setHeaderData(self.routing_number_col, Qt.Horizontal, 'Routing #')

    def insertRows(self, before_row, count, parent=None):
        for i in range(count):
            self.budget.accounts.append(
                AccountItem.from_values(self.budget,
                                        name='New Account {}'.format(i+1),
                                        hidden=False,
                                        routing_number=None,
                                        account_number=None
                ))

        return True

    def data(self, index, role=Qt.DisplayRole):
        col = index.column()
        account = self.budget.accounts[self.get_id_for_index(index)]

        if col == self.id_col:
            if role == Qt.DisplayRole or role == Qt.EditRole:
                return account.id
        elif col == self.is_hidden_col:
            if role == Qt.CheckStateRole:
                is_hidden = account.hidden
                if not is_hidden:
                    return Qt.Checked
                else:
                    return Qt.Unchecked
            elif role == Qt.DisplayRole:
                return '' # TODO it would be nice we we could center the checkbox
        elif col == self.name_col:
            if role == Qt.DisplayRole or role == Qt.EditRole:
                return account.name
        elif col == self.account_number_col:
            if role == Qt.DisplayRole or role == Qt.EditRole:
                return account.account_number
        elif col == self.routing_number_col:
            if role == Qt.DisplayRole or role == Qt.EditRole:
                return account.routing_number

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        col = index.column()
        account = self.budget.accounts.item_for_index(index.row())

        if col == self.is_hidden_col and role == Qt.CheckStateRole:
            account.show(value == Qt.Checked)
            account.save()
            return True
        elif col == self.name_col and role == Qt.EditRole:
            account.name = value
            account.save()
            return True
        elif col == self.account_number_col and role == Qt.EditRole:
            account.account_number = value
            account.save()
            return True
        elif col == self.routing_number_col and role == Qt.EditRole:
            account.routing_number = value
            account.save()
            return True
        else:
            return super(AccountsModel, self).data(index, role)

    def flags(self, index):
        col = index.column()
        if col == self.is_hidden_col:
            return Qt.ItemIsEnabled | Qt.ItemIsSelectable | Qt.ItemIsUserCheckable
        elif (col == self.name_col or
              col == self.account_number_col or
              col == self.routing_number_col):
            return Qt.ItemIsEnabled | Qt.ItemIsEditable | Qt.ItemIsSelectable
        else:
            return super(AccountsModel, self).flags(index)
