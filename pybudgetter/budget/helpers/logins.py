from pprint import pprint

from PyQt5.QtCore import QObject

from .. import errors

class LoginContainer:
    """Presents a nice interface to work with the logins in the current database"""
    def __init__(self, budget):
        self.budget = budget

        self.rebuild_cache()
        self.budget.logins_changed.connect(self.rebuild_cache)

    def rebuild_cache(self):
        self.__cache = []

        if self.budget.is_open():
            query = self.query()
            if not query.prepare('SELECT id FROM logins'):
                raise errors.BudgetError('Unable to get login IDs', query.lastError())

            if not query.exec_():
                raise errors.BudgetError('Unable to get login IDs', query.lastError())

            # TODO create BudgetLogin objects
            ids = list()
            while query.next():
                ids.append(query.value(0))

            self.__cache = ids

    def __len__(self):
        return len(self.__cache)

    def __getitem__(self, id):
        return self.__cache[id]

class BudgetLogin:
    def __init__(self, budget):
        self.budget = budget

    @classmethod
    def fromdatabase(cls, budget, id):
        pass

class BudgetLoginSetting(QObject):
    """
    Handles working with login settings
    """

    ERROR_KEY = '__error_msg'
    USERNAME_KEY = 'user'
    PASSWORD_KEY = 'password'
    PIN_KEY = 'pin'

    def __init__(self, budget, setting_id=None, login_id=None, key=None):
        super(BudgetLoginSetting, self).__init__()

        self.__budget = budget

        if setting_id is not None:
            # Given ID directly
            self.__setting_id = setting_id
        elif login_id is not None and key is not None:
            # Must find setting ID
            query = self.budget.query()
            if not query.prepare('SELECT id FROM login_settings WHERE login_id=? AND key=?'):
                raise errors.BudgetError('Unable to get login setting', query.lastError())

            query.addBindValue(login_id)
            query.addBindValue(key)

            if not query.exec_():
                raise errors.BudgetError('Unable to get login setting', query.lastError())

            if not query.next():
                raise errors.BudgetNoSuchRowError('Unable to get login setting',
                                                  'Login ID/key pair not found ({}/{})'.format(login_id, key))

            self.__setting_id = query.value(0)
        else:
            raise errors.BudgetError('Parameters incorrect to BudgetLoginSetting')

    def __get_login_setting_details(self):
        """Fetches all details on setting. Returns them as a tuple """
        query = self.__budget.query()
        if not query.prepare('SELECT id, login_id, key, value, prompt, needed FROM login_settings WHERE id=?'):
            raise errors.BudgetError('Unable to get login setting', query.lastError())

        query.addBindValue(self.__setting_id)

        if not query.exec_():
            raise errors.BudgetError('Unable to get login setting', query.lastError())

        if not query.next():
            raise errors.BudgetNoSuchRowError('Unable to get login setting', 'Login setting not found')

        values = list()
        for i in range(30):
            v = query.value(i)
            if v is None:
                break

            values.append(v)

        return values

    def __set_login_setting_details(self, key, value):
        """Sets a specific piece of the login setting. UNSAFE! column name is used unprepared"""
        query = self.__budget.query()
        if not query.prepare('UPDATE login_settings SET {}=? WHERE id=?'.format(key)):
            raise errors.BudgetError('Unable to change login setting {}'.format(key), query.lastError())

        query.addBindValue(value)
        query.addBindValue(self.__setting_id)

        if not query.exec_():
            raise errors.BudgetError('Unable to change login setting {}'.format(key), query.lastError())

        self.__budget.logins_changed.emit()

    def get_id(self):
        """Returns the ID of this setting"""
        return self.__setting_id

    def get_login_id(self):
        """Returns the login id for this setting """
        return self.__get_login_setting_details()[1]

    def get_key(self):
        """Returns the key for this login setting """
        return self.__get_login_setting_details()[2]

    def get_value(self):
        """Returns the value for this login setting """
        return self.__get_login_setting_details()[3]

    def set_value(self, value):
        """Sets the value for this login setting"""
        return self.__set_login_setting_details('value', value)

    def get_prompt(self):
        """Returns the prompt for this login setting """
        return self.__get_login_setting_details()[4]

    def set_prompt(self, prompt):
        """Sets the prompt for the setting"""
        return self.__set_login_setting_details('prompt', prompt)

    def is_needed(self):
        """Returns true if a value is needed for this login setting """
        return self.__get_login_setting_details()[5]

    def set_needed(self, needed=True):
        """Sets whether this setting is needed from the user or not. Defaults to marking as needed."""
        return self.__set_login_setting_details('needed', needed)

    def is_user_setting(self):
        """Returns true if this is a setting intended for the user to change """
        key = self.get_key()
        return not key.startswith('__')
