from pprint import pprint

from PyQt5.QtCore import *

from . import base
from .. import errors

class CategoryItem(base.CachingItemHelper):
    def __init__(self, budget, id=None):
        super(CategoryItem, self).__init__(budget, budget.categories, id)

        self.__total_cache = None
        self.__from_cache = None
        self.__to_cache = None

    @property
    def cached_attributes(self):
        return ('name', 'amount')

    def _bind_to_budget(self):
        if super(CategoryItem, self)._bind_to_budget():
            # Retotal as needed
            self.budget.transactions.changed.connect(self.on_transaction_change)
            self.budget.transactions.removing.connect(self.on_transaction_removing)
            self.budget.transactions.removed.connect(self.on_transaction_change)
            self.budget.transactions.added.connect(self.on_transaction_change)
            return True
        else:
            return False

    def _unbind_to_budget(self):
        if super(CategoryItem, self)._unbind_to_budget():
            self.budget.transactions.changed.disconnect(self.on_transaction_change)
            self.budget.transactions.removing.disconnect(self.on_transaction_removing)
            self.budget.transactions.removed.disconnect(self.on_transaction_change)
            self.budget.transactions.added.disconnect(self.on_transaction_change)
            return True
        else:
            return False

    @pyqtSlot(object)
    def on_transaction_removing(self, trans):
        if self.id == trans.category_id:
            self.__clear_total_cache()

    @pyqtSlot(object)
    def on_transaction_change(self, trans):
        if self.__total_cache is None or self.id == trans.category_id:
            # IF this transaction change resulted in a new total, emit that we
            # changed. Otherwise, ignore it
            old = self.__total_cache
            if old is None:
                return

            self.__clear_total_cache()
            new = self.total(self.__from_cache, self.__to_cache)

            if new != old:
                self.budget.categories.changed.emit(self)

    def _load_from_db(self):
        """Loads/reloads the account data from the database"""
        if not self.budget.is_open():
            self.cache = None
            return

        query = self.budget.query()
        if not query.prepare('SELECT name, amount FROM categories WHERE id=?'):
            raise errors.BudgetError('Unable to load category from database', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to load category from database', query.lastError())
        if not query.next():
            raise errors.BudgetNoSuchRowError('Unable to load category with ID {} not found'.format(self.id))

        self.name = query.value(0)
        self.amount = query.value(1)

    def _save_new(self):
        query = self.budget.query()
        if not query.prepare('INSERT INTO categories (name, amount) VALUES (?, ?)'):
            raise errors.BudgetError('Unable to create category', query.lastError())

        query.addBindValue(self.name)
        query.addBindValue(self.amount)

        if not query.exec_():
            raise errors.BudgetError('Unable to create category', query.lastError())

        self.id = query.lastInsertId()

    def _save_existing(self):
        query = self.budget.query()
        if not query.prepare('''UPDATE categories SET name=?,
                                                      amount=?
                      WHERE id=?'''):
            raise errors.BudgetError('Unable to update category', query.lastError())

        query.addBindValue(self.name)
        query.addBindValue(self.amount)
        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to update category', query.lastError())

    def _delete_from_db(self):
        query = self.budget.query()
        if not query.prepare('''DELETE FROM categories WHERE id=?'''):
            raise errors.BudgetError('Unable to remove category', query.lastError())

        query.addBindValue(self.id)

        if not query.exec_():
            raise errors.BudgetError('Unable to remove category', query.lastError())

    def __clear_total_cache(self, item=None):
        self.__total_cache = None

    def total(self, from_date, to_date):
        """Totals the transactions under this category between the given dates.
        Dates should be given as Qt QDate objects."""
        if self.__total_cache is not None and from_date == self.__from_cache and to_date == self.__to_cache:
            return self.__total_cache

        query = self.budget.query()
        if not query.prepare("""SELECT sum(t.amount) FROM transactions t
                                    JOIN accounts a ON a.id=t.account_id AND a.hidden=0
                                    WHERE t.category_id=?
                                        AND t.date BETWEEN ? AND ?"""):
            raise errors.BudgetError('Unable to total transactions under category', query.lastError())

        query.addBindValue(self.id)
        query.addBindValue(from_date.toString(Qt.ISODate))
        query.addBindValue(to_date.toString(Qt.ISODate))

        if not query.exec_():
            raise errors.BudgetError('Unable to total transactions under category', query.lastError())
        if not query.next():
            raise errors.BudgetError('Unable to total transactions under category', query.lastError())

        total = query.value(0)
        if total:
            total = float(total)
        else:
            total = 0.0

        self.__total_cache = total
        self.__from_cache = from_date
        self.__to_cache = to_date
        return self.__total_cache

class CategoriesContainer(base.CachingItemContainer):
    """Presents a clean interface to work with the categories in the current database"""
    def __init__(self, budget):
        super(CategoriesContainer, self).__init__(budget)

    def rebuild_cache(self):
        self.cache = {}

        if self.budget.is_open():
            query = self.budget.query()
            if not query.prepare('SELECT id FROM categories'):
                raise errors.BudgetError('Unable to get category list from database', query.lastError())

            if not query.exec_():
                raise errors.BudgetError('Unable to get category list from database', query.lastError())

            while query.next():
                self.cache[query.value(0)] = CategoryItem.from_id(self.budget, query.value(0))

    def get_income_total(self):
        """Returns the total af all postitive categories (ie, income categories)"""
        if not self.budget.is_open():
            return 0.0

        query = self.budget.query()
        if not query.prepare('SELECT sum(amount) FROM categories WHERE amount > 0'):
            raise errors.BudgetError('Unable to get category income total', query.lastError())

        if not query.exec_():
            raise errors.BudgetError('Unable to get category income total', query.lastError())

        if not query.next():
            raise errors.BudgetError('Unable to get category income total', query.lastError())

        val = query.value(0)
        if val:
            return val
        else:
            return 0.0

    def get_expenses_total(self):
        """Returns the total af all negative categories (ie, expense categories)"""
        if not self.budget.is_open():
            return 0.0

        query = self.budget.query()
        if not query.prepare('SELECT sum(amount) FROM categories WHERE amount < 0'):
            raise errors.BudgetError('Unable to get category expense total', query.lastError())

        if not query.exec_():
            raise errors.BudgetError('Unable to get category expense total', query.lastError())

        if not query.next():
            raise errors.BudgetError('Unable to get category expense total', query.lastError())

        val = query.value(0)
        if val:
            return val
        else:
            return 0.0

class CategoryModel(base.BudgetDrivenModel):
    def __init__(self, budget):
        super(CategoryModel, self).__init__(budget,
            budget.categories,
            ('id', 'desc', 'amount', 'used'))

        self.__to_date = QDate.currentDate().addMonths(6)
        self.__from_date = self.__to_date.addMonths(-12)

        # Headers
        self.setHeaderData(self.id_col, Qt.Horizontal, 'ID')
        self.setHeaderData(self.desc_col, Qt.Horizontal, 'Description')
        self.setHeaderData(self.amount_col, Qt.Horizontal, 'Allocated')
        self.setHeaderData(self.used_col, Qt.Horizontal, 'Used')

    def data(self, index, role=Qt.DisplayRole):
        col = index.column()
        cat = self.budget.categories[self.get_id_for_index(index)]

        if role == Qt.DisplayRole or role == Qt.EditRole:
            if col == self.id_col:
                return cat.id
            elif col == self.desc_col:
                return cat.name
            elif col == self.amount_col:
                return cat.amount
            elif col == self.used_col:
                return cat.total(self.__from_date, self.__to_date)

        return QVariant()

    def setData(self, index, value, role=Qt.EditRole):
        col = index.column()
        cat = self.budget.categories[self.get_id_for_index(index)]

        if col == self.desc_col:
            cat.name = value
            cat.save()
            return True
        elif col == self.amount_col:
            cat.amount = value
            cat.save()
            return True
        else:
            return True

    def flags(self, index):
        col = index.column()
        if (col == self.desc_col or
            col == self.amount_col):
            return Qt.ItemIsEnabled | Qt.ItemIsEditable | Qt.ItemIsSelectable
        else:
            return super(CategoryModel, self).flags(index)

    def insertRows(self, before_row, count, parent=None):
        for i in range(count):
            self.budget.categories.append(
                CategoryItem.from_values(self.budget,
                    name='New Category {}'.format(i+1),
                    amount=0.00
                ))

        return True

    def set_to_date(self, date):
        self.__to_date = date

        # Used column values have changed!
        tl = self.index(0, self.used_col)
        br = self.index(self.rowCount(), self.used_col)
        self.dataChanged.emit(tl, br)

    def set_from_date(self, date):
        self.__from_date = date

        # Used column values have changed!
        tl = self.index(0, self.used_col)
        br = self.index(self.rowCount(), self.used_col)
        self.dataChanged.emit(tl, br)
