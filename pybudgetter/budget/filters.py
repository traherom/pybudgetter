import re
from pprint import pprint

from PyQt5.QtCore import Qt

from . import utilities
from . import errors

class TransactionFilter:
    def __init__(self, budget, id=None):
        self.__budget = budget
        self.__id = id

    def filter_model(self, model, row):
        """Returns True if the row in the given model should be
        included (i.e., the filter matches) and False otherwise."""
        raise NotImplementedError('filter_model not implemented')

    def filter_transaction(self, transaction):
        """Returns True if the given transation passes this filter (matches),
        False otherwise."""
        raise NotImplementedError('filter_transaction not implemented')

    @classmethod
    def load(cls, budget, id):
        """Loads the filter for the given ID, creating the correct
        class for the rule"""
        raise NotImplementedError('load() must be implemented')

    def save(self, rule_id):
        """Saves the filter to the database"""
        if self.__budget is None:
            raise errors.BudgetError('Unable to save filter, budget not set')

        query = self.__budget.query()
        if not query.prepare("""INSERT INTO rule_filters (rule_id, filter_class)
                                    VALUES (?, ?)"""):
            raise errors.BudgetError('Unable to save new filter to database', query.lastError())

        query.addBindValue(rule_id)
        t = str(type(self))[8:-2]
        query.addBindValue(t)

        if not query.exec_():
            raise errors.BudgetError('Unable to save new filter to database', query.lastError())

        self.__id = query.lastInsertId()

        # Settings
        self.__save_settings()

    def __save_settings(self):
        # Save all settings
        query = self.__budget.query()
        if not query.prepare('INSERT INTO filter_settings (filter_id, key, value) VALUES (?, ?, ?)'):
            raise errors.BudgetError('Unable to create filter settings insertion', query.lastError())

        query.bindValue(0, self.__id)

        sets = self.get_settings()
        for k, v in sets.items():
            query.bindValue(1, k)
            query.bindValue(2, v)

            if not query.exec_():
                raise errors.BudgetError('Unable to insert filter setting {}'.format(k),
                        query.lastError())

    def get_settings(self):
        """Returns filter settings as a dictionary"""
        raise NotImplementedError('get_settings must be implemented')

    @staticmethod
    def filter_settings(budget, id):
        """Returns all settings for the given filter from the database"""
        query = budget.query()
        if not query.prepare('SELECT key, value FROM filter_settings WHERE filter_id=?'):
            raise errors.BudgetError('Unable to load filter from database', query.lastError())

        query.addBindValue(id)

        if not query.exec_():
            raise errors.BudgetError('Unable to load filter from database', query.lastError())

        kv = {}
        while query.next():
            kv[query.value(0)] = query.value(1)

        return kv

    def __str__(self):
        return "FILTER"

class UncategorizedFilter(TransactionFilter):
    def __init__(self, budget, id=None):
        super(UncategorizedFilter, self).__init__(budget, id)

    def filter_model(self, model, row):
        cat_id = model.index(row, model.cat_col).data()
        return not (type(cat_id) == int)

    def filter_transaction(self, transaction):
        if transaction.category_id is None:
            return True
        else:
            return False

    @classmethod
    def load(cls, budget, id):
        return cls(budget, id=id)

    def get_settings(self):
        # No settings needed
        return {}

    def __str__(self):
        return 'is uncategorized'

class CategorizedFilter(TransactionFilter):
    def __init__(self, budget, id=None):
        super(CategorizedFilter, self).__init__(budget, id)

    def filter_model(self, model, row):
        cat_id = model.index(row, model.cat_col).data()
        return (type(cat_id) == int)

    def filter_transaction(self, transaction):
        if transaction.get_category_id() is not None:
            return True
        else:
            return False

    @classmethod
    def load(cls, budget, id):
        return cls(budget, id=id)

    def get_settings(self):
        # No settings needed
        return {}

    def __str__(self):
        return 'is categorized'

class DayFilter(TransactionFilter):
    """Filters transactions based on the day in the month, inclusive."""
    def __init__(self, budget, from_day, to_day, id=None):
        """to_day and from_day should be numercial days"""
        super(DayFilter, self).__init__(budget, id)
        self.to_day = to_day
        self.from_day = from_day

    @property
    def to_day(self):
        return self.__to

    @to_day.setter
    def to_day(self, value):
        self.__to = int(value)

    @property
    def from_day(self):
        return self.__from

    @from_day.setter
    def from_day(self, value):
        self.__from = int(value)

    @classmethod
    def load(cls, budget, id):
        kv = TransactionFilter.filter_settings(budget, id)
        return cls(budget, kv['from'], kv['to'], id=id)

    def get_settings(self):
        return {'to': self.__to, 'from': self.__from}

    def filter_model(self, model, row):
        day = model.index(row, model.date_col).data()
        day = day.day

        # Do reversed comparison in case the user put the filter in backward
        if self.__from <= day <= self.__to:
            return True
        elif self.__to <= day <= self.__from:
            return True
        else:
            return False

    def filter_transaction(self, transaction):
        date = transaction.date

        # Do reversed comparison as well in case the user put the filter in backward
        if self.__from <= date.day <= self.__to:
            return True
        elif self.__to <= date.day <= self.__from:
            return True
        else:
            return False

    def __str__(self):
        return 'day {} to {}'.format(self.__from, self.__to)

class AmountFilter(TransactionFilter):
    def __init__(self, budget, from_amt, to_amt, id=None):
        super(AmountFilter, self).__init__(budget, id)
        self.to_amt = to_amt
        self.from_amt = from_amt

    @property
    def to_amt(self):
        return self.__to

    @to_amt.setter
    def to_amt(self, value):
        self.__to = float(value)

    @property
    def from_amt(self):
        return self.__from

    @from_amt.setter
    def from_amt(self, value):
        self.__from = float(value)

    @classmethod
    def load(cls, budget, id):
        kv = TransactionFilter.filter_settings(budget, id)
        return cls(budget, kv['from'], kv['to'], id=id)

    def get_settings(self):
        return {'to': self.__to, 'from': self.__from}

    def filter_model(self, model, row):
        amt = model.index(row, model.amount_col).data(Qt.EditRole)
        if self.__from <= amt <= self.__to:
            return True
        elif self.__to <= amt <= self.__from:
            return True
        else:
            return False

    def filter_transaction(self, transaction):
        amount = transaction.amount
        if self.__from <= amount <= self.__to:
            return True
        elif self.__to <= amount <= self.__from:
            return True
        else:
            return False

    def __str__(self):
        return 'amount {} to {}'.format(utilities.string_dollars(self.__from),
                                     utilities.string_dollars(self.__to))

class DescriptionFilter(TransactionFilter):
    def __init__(self, budget, regex, id=None):
        """If given a string, it will be compiled case insensitively. To avoid
        that, pass a compiled regex."""
        super(DescriptionFilter, self).__init__(budget, id)

        self.regex = regex

    @property
    def regex(self):
        return self.__regex.pattern

    @regex.setter
    def regex(self, value):
        if type(value) == str:
            # Compile it ourselves
            self.__regex = re.compile(value, re.IGNORECASE)
        else:
            self.__regex = value

    @classmethod
    def load(cls, budget, id):
        kv = TransactionFilter.filter_settings(budget, id)
        return cls(budget, kv['regex'], id=id)

    def get_settings(self):
        return {'regex': self.__regex.pattern}

    def filter_model(self, model, row):
        desc = model.index(row, model.desc_col).data()

        if self.__regex.search(desc) is not None:
            return True
        else:
            return False

    def filter_transaction(self, transaction):
        desc = transaction.description
        if self.__regex.search(desc) is not None:
            return True
        else:
            return False

    def __str__(self):
        return 'matches /{}/'.format(self.__regex.pattern)
