import csv
from pprint import pprint

from .helpers import transactions
from ofxparse import ofxparse

class ImportedAccount:
    """Simple for used by parsers to represent the accounts they find"""
    def __init__(self, budget, transactions, account_num=None, routing_num=None,
                 atype='Unknown'):
        self.budget = budget
        self.transactions = transactions
        self.account_number = account_num
        self.routing_number = routing_num
        self.type = atype

    def perform_background_import(self, local_acc):
        """Passes the import request off to the budget background processor"""
        self.budget.perform_background_task(self.perform_import, local_acc)
        #self.perform_import(local_acc)

    def perform_import(self, local_acc):
        """Imports the imported account's transactions into the local account"""
        total = len(self.transactions)
        self.budget.activity_start.emit(total)
        self.budget.activity_progress.emit(0, 'Importing transactions')

        total_imported = 0
        total_checked = 0

        try:
            self.budget.start_transaction()

            to_import = []

            for t in self.transactions:
                # If this transaction does not exist already, import it
                # TODO are we guaranteed to get transactions in newest to oldest order?
                # would be a nice way to stop search early. Once we hit one that exists,
                # we can stop entirely
                total_checked += 1
                self.budget.activity_progress.emit(total_checked,
                    'Importing transaction {}/{} to account {}'.format(total_checked, total, local_acc.name))

                if self.budget.transactions.find(description=t.description, date=t.date, amount=t.amount) is None:
                    total_imported += 1

                    trans = transactions.TransactionItem.from_values(self.budget,
                                                                     account_id=local_acc.id,
                                                                     date=t.date,
                                                                     description=t.description,
                                                                     amount=t.amount,
                                                                     category_id=None)
                    trans.apply_rules()
                    trans.save_no_append()

                    to_import.append(trans)

            self.budget.commit()

            # Save all the transactions to the DB then reset the container. Simpler on huge imports
            self.budget.transactions.append_list(to_import)
            self.budget.activity_progress.emit(0, 'Import complete')
        except:
            self.budget.rollback()
            self.budget.activity_progress.emit(0, 'Import failed')
            raise
        finally:
            self.budget.activity_end.emit()

        return total_imported

    def __str__(self):
        s = 'Account {}:{} with {} transactions:\n'.format(self.account_number,
                                                         self.routing_number,
                                                         len(self.transactions))
        s += '\n\t'.join([str(t) for t in self.transactions])
        return s

class ImportedTransaction:
    """Simple for used by parsers to represent the transactions they find"""
    def __init__(self, budget, date, description, amount):
        self.budget = budget
        self.date = date
        self.description = description
        self.amount = amount

    def __str__(self):
        return '{} {} {}'.format(self.date, self.description, self.amount)

class OFXInterpretter:
    def __init__(self, budget, infile):
        self.budget = budget
        self.ofx = ofxparse.OfxParser.parse(infile)

        self.accounts = []
        for a in self.ofx.accounts:
            transactions = []
            for t in a.statement.transactions:
                transactions.append(ImportedTransaction(budget, t.date,
                                                        t.payee, t.amount))

            imported = ImportedAccount(budget,
                                       transactions,
                                       a.account_id,
                                       a.routing_number,
                                       a.account_type)


            self.accounts.append(imported)

    @classmethod
    def from_file(cls, budget, inpath):
        with open(inpath, 'rb') as infile:
            return cls(budget, infile)

class CSVInterpretter:
    def __init__(self, budget, infile, columns=None):
        """Reads in the given CSV file. If columns is None, attempts to determine
        what the CSV columns are based on file contents."""
        self.budget = budget
        self.csv = csv.reader(infile)

    @classmethod
    def from_file(cls, budget, inpath):
        with open(inpath, 'r', newline='') as infile:
            return cls(budget, infile)

def from_file(budget, inpath):
    if inpath.endswith('.ofx'):
        return OFXInterpretter.from_file(budget, inpath)
    elif inpath.endswith('.csv'):
        return CSVInterpretter.from_file(budget, inpath)
    else:
        raise TypeError('File type not supported for importing')
