from PyQt5.QtCore import Qt, QSortFilterProxyModel

from ..budget import utilities, filters

class SimpleProxyModel(QSortFilterProxyModel):
    """Defines very basics for a proxy model and passes unknown
    functions calls/attribute requests on to the base model"""
    def __init__(self, budget, base_model):
        super(SimpleProxyModel, self).__init__()
        self.budget = budget

        self.model = base_model
        self.setSourceModel(self.model)

    def __getattr__(self, name):
        """Passes calls off to the base model if not overriden by
        children"""
        return getattr(self.model, name)

class AccountsProxyModel(SimpleProxyModel):
    def __init__(self, budget):
        super(AccountsProxyModel, self).__init__(budget, budget.create_accounts_model())

class TransactionProxyModel(SimpleProxyModel):
    def __init__(self, budget):
        super(TransactionProxyModel, self).__init__(budget, budget.create_transactions_model())

        self.__uncat_rule = None
        self.__rules = list()
        self.setFilterKeyColumn(self.desc_col)
        self.setFilterCaseSensitivity(Qt.CaseInsensitive)

    def show_uncategorized_only(self, uncat_only=True):
        """Adds an UncategorizedFilter rule to the model. clear_filters
        will remove it."""
        if uncat_only and self.__uncat_rule is None:
            self.__uncat_rule = filters.UncategorizedFilter(self.budget)
            self.add_filter(self.__uncat_rule)
        elif self.__uncat_rule is not None:
            self.remove_filter(self.__uncat_rule)
            self.__uncat_rule = None

    @property
    def filters(self):
        """Returns all rules currently on model"""
        return self.__rules

    @filters.setter
    def filters(self, rules):
        """Adds all rules to model"""
        self.__rules = list(rules)
        self.invalidateFilter()

    @filters.deleter
    def filters(self):
        """Clears all current filters"""
        self.clear_filters()

    def add_filter(self, rule):
        self.__rules.append(rule)
        self.invalidateFilter()

    def remove_filter(self, rule):
        self.__rules.remove(rule)
        self.invalidateFilter()

    def clear_filters(self):
        """Clears all filters from proxy"""
        self.__rules = list()
        self.__uncat_rule = None
        self.invalidateFilter()

    def filterAcceptsRow(self, row, parent):
        source = self.sourceModel()

        # Go through all the filters
        for rule in self.__rules:
            if not rule.filter_model(source, row):
                return False

        return super(TransactionProxyModel, self).filterAcceptsRow(row, parent)

    def data(self, index, role=Qt.DisplayRole):
        # Get default value
        default = super(TransactionProxyModel, self).data(index, role)

        if index.column() == self.amount_col:
            if role == Qt.DisplayRole:
                return utilities.string_dollars(default)
            elif role == Qt.ForegroundRole:
                val = super(TransactionProxyModel, self).data(index, Qt.EditRole)
                return utilities.dollars_brush(val)
        elif index.column() == self.date_col and role == Qt.DisplayRole:
            # Only show the date, not the time.  Locale-dependent representation
            return default.strftime('%x')

        # Use parent's data()
        return default

class CategoryProxyModel(SimpleProxyModel):
    def __init__(self, budget):
        super(CategoryProxyModel, self).__init__(budget, budget.create_category_model())

    def data(self, index, role=Qt.DisplayRole):
        default = super(CategoryProxyModel, self).data(index, role)

        if index.column() == self.amount_col or index.column() == self.used_col:

            if role == Qt.DisplayRole:
                return utilities.string_dollars(default)
            elif role == Qt.ForegroundRole:
                # Green positive amounts, red negative
                val = super(CategoryProxyModel, self).data(index, Qt.EditRole)
                return utilities.dollars_brush(val)

        # If unhandled elsewhere, use our parent's data() value
        return default
