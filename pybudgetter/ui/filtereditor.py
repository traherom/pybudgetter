from PyQt5.QtWidgets import (QWidget, QHBoxLayout, QLabel,
                             QSpinBox, QPushButton, QDoubleSpinBox,
                             QLineEdit, QComboBox)
from PyQt5.QtCore import QFile, QIODevice, pyqtSlot, pyqtSignal
from PyQt5 import uic

from ..budget import filters

class FilterEditor(QWidget):
    def __init__(self, budget, parent=None):
        super(FilterEditor, self).__init__(parent)

        self.__budget = budget

        file = QFile(':filterwidget.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open filter editor UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        self.__rows = list()
        self.__extra_btns = list()

    def clear(self):
        """Clears all rules"""
        while self.__rows:
            self.__rows.pop().deleteLater()

    @property
    def filters(self):
        """Returns a list of TransactionFilters that the user has set up"""
        rules = list()
        for row in self.__rows:
            rule = row.filter
            if rule is not None:
                rules.append(rule)

        return rules

    @filters.setter
    def filters(self, filters):
        """Sets the filter editor to the given filters, changing the UI as needed"""
        self.clear()

        for filter in filters:
            row = FilterEditorRow.from_filter(self.__budget, filter)
            row.removed.connect(self.on_row_remove)
            self.__rows.append(row)
            self.ui.editorRowsLayout.insertWidget(
                self.ui.editorRowsLayout.count() - 1, row)

    def add_extra_button(self, btn):
        """Accepts a button that gets placed below the Add Filter button.
        Can be used by called to create a save or apply button, for example"""
        self.__extra_btns.append(btn)
        self.ui.extraLayout.insertWidget(
            self.ui.extraLayout.count(), btn)

    def setEnabled(self, enabled):
        """Handles enabling all components"""
        super(FilterEditor, self).setEnabled(enabled)

        # All the rows and extra buttons
        for row in self.__rows:
            row.setEnabled(enabled)

        for btn in self.__extra_btns:
            btn.setEnabled(enabled)

        # General buttons
        self.ui.addBtn.setEnabled(enabled)
        self.ui.filterTypeBox.setEnabled(enabled)

    def setDisabled(self, disabled):
        self.setEnabled(not disabled)

    @pyqtSlot()
    def on_addBtn_clicked(self):
        """Add a row for the selected filter type"""
        type = self.ui.filterTypeBox.currentText()

        if type == 'Day Between':
            row = DayFilterEditor(self.__budget, parent=self)
        elif type == 'Amount':
            row = AmountFilterEditor(self.__budget, parent=self)
        elif type == 'Description':
            row = DescriptionFilterEditor(self.__budget, parent=self)
        elif type == 'Categorized?':
            row = UncategorizedFilterEditor(self.__budget, parent=self)
        else:
            raise ValueError('Unexpected value in filter type box')

        row.removed.connect(self.on_row_remove)
        self.__rows.append(row)
        self.ui.editorRowsLayout.insertWidget(
            self.ui.editorRowsLayout.count() - 1, row)

    @pyqtSlot(object)
    def on_row_remove(self, row):
        self.__rows.remove(row)
        row.deleteLater()

class FilterEditorRow(QWidget):
    removed = pyqtSignal(object)

    def __init__(self, budget, parent=None):
        super(FilterEditorRow, self).__init__(parent)
        self.__budget = budget

    @property
    def filter(self):
        """Returns the rule with the settings from this widget. If
        the user hasn't supplied sufficient data yet, None is returned"""
        raise NotImplementedError('Children must implement filter')

    @classmethod
    def from_filter(cls, budget, filter):
        """Creates the correct editor row based on the given filter's type"""
        if isinstance(filter, filters.UncategorizedFilter):
            return UncategorizedFilterEditor(budget, filter)
        elif isinstance(filter, filters.CategorizedFilter):
            return UncategorizedFilterEditor(budget, filter)
        elif isinstance(filter, filters.DayFilter):
            return DayFilterEditor(budget, filter)
        elif isinstance(filter, filters.AmountFilter):
            return AmountFilterEditor(budget, filter)
        elif isinstance(filter, filters.DescriptionFilter):
            return DescriptionFilterEditor(budget, filter)
        else:
            raise ValueError('Unexpected filter type given, unable ot create editor row')

    @pyqtSlot()
    def on_remove_clicked(self):
        self.removed.emit(self)

class DayFilterEditor(FilterEditorRow):
    def __init__(self, budget, filter=None, parent=None):
        super(DayFilterEditor, self).__init__(parent)

        self.__budget = budget
        self.__filter = filter

        layout = QHBoxLayout()
        self.setLayout(layout)
        layout.setContentsMargins(0, 0, 0, 0)

        layout.addStretch()
        layout.addWidget(QLabel('Day from:'))

        self.fromSpin = QSpinBox()
        self.fromSpin.setRange(1, 31)
        layout.addWidget(self.fromSpin)

        layout.addWidget(QLabel('To:'))

        self.toSpin = QSpinBox()
        self.toSpin.setRange(1, 31)
        layout.addWidget(self.toSpin)

        if self.__filter is None:
            self.toSpin.setValue(31)
            self.fromSpin.setValue(1)
        else:
            self.toSpin.setValue(int(self.__filter.to_day))
            self.fromSpin.setValue(int(self.__filter.from_day))

        self.removeBtn = QPushButton('Remove')
        self.removeBtn.clicked.connect(self.on_remove_clicked)
        layout.addWidget(self.removeBtn)

    @property
    def filter(self):
        if self.__filter is None:
            return filters.DayFilter(self.__budget, self.fromSpin.value(), self.toSpin.value())
        else:
            self.__filter.to_day = self.toSpin.value()
            self.__filter.from_day = self.fromSpin.value()
            return self.__filter

class AmountFilterEditor(FilterEditorRow):
    def __init__(self, budget, filter=None, parent=None):
        super(AmountFilterEditor, self).__init__(parent)

        self.__budget = budget
        self.__filter = filter

        layout = QHBoxLayout()
        self.setLayout(layout)
        layout.setContentsMargins(0, 0, 0, 0)

        layout.addStretch()
        layout.addWidget(QLabel('Amount from:'))

        self.fromSpin = QDoubleSpinBox()
        self.fromSpin.setPrefix('$')
        self.fromSpin.setRange(-1000000, 1000000)
        layout.addWidget(self.fromSpin)

        layout.addWidget(QLabel('To:'))

        self.toSpin = QDoubleSpinBox()
        self.toSpin.setPrefix('$')
        self.toSpin.setRange(-1000000, 1000000)
        layout.addWidget(self.toSpin)

        if self.__filter is None:
            self.toSpin.setValue(0.0)
            self.fromSpin.setValue(0.0)
        else:
            self.toSpin.setValue(float(self.__filter.to_amt))
            self.fromSpin.setValue(float(self.__filter.from_amt))

        self.removeBtn = QPushButton('Remove')
        self.removeBtn.clicked.connect(self.on_remove_clicked)
        layout.addWidget(self.removeBtn)

    @property
    def filter(self):
        if self.__filter is None:
            return filters.AmountFilter(self.__budget, self.fromSpin.value(), self.toSpin.value())
        else:
            self.__filter.to_amt = self.toSpin.value()
            self.__filter.from_amt = self.fromSpin.value()
            return self.__filter

class DescriptionFilterEditor(FilterEditorRow):
    def __init__(self, budget, filter=None, parent=None):
        super(DescriptionFilterEditor, self).__init__(parent)

        self.__budget = budget
        self.__filter = filter

        layout = QHBoxLayout()
        self.setLayout(layout)
        layout.setContentsMargins(0, 0, 0, 0)

        layout.addStretch()
        layout.addWidget(QLabel('Description:'))

        self.desc = QLineEdit()
        layout.addWidget(self.desc)

        if self.__filter is None:
            self.desc.setText('.*')
        else:
            self.desc.setText(filter.regex)

        self.removeBtn = QPushButton('Remove')
        self.removeBtn.clicked.connect(self.on_remove_clicked)
        layout.addWidget(self.removeBtn)

    @property
    def filter(self):
        if self.__filter is None:
            return filters.DescriptionFilter(self.__budget, self.desc.text())
        else:
            self.__filter.regex = self.desc.text()
            return self.__filter

class UncategorizedFilterEditor(FilterEditorRow):
    # TODO break into cat/uncat filter editor rows
    def __init__(self, budget, filter=None, parent=None):
        super(UncategorizedFilterEditor, self).__init__(parent)

        self.__budget = budget
        self.__filter = filter

        layout = QHBoxLayout()
        self.setLayout(layout)
        layout.setContentsMargins(0, 0, 0, 0)

        layout.addStretch()
        layout.addWidget(QLabel('Show only:'))

        self.type = QComboBox()
        self.type.addItem('Uncategorized')
        self.type.addItem('Categorized')
        self.type.setCurrentIndex((0 if isinstance(filter, filters.CategorizedFilter) else 1))
        layout.addWidget(self.type)

        self.removeBtn = QPushButton('Remove')
        self.removeBtn.clicked.connect(self.on_remove_clicked)
        layout.addWidget(self.removeBtn)

    @property
    def filter(self):
        if self.type.currentText() == 'Uncategorized':
            return filters.UncategorizedFilter(self.__budget)
        else:
            return filters.CategorizedFilter(self.__budget)
