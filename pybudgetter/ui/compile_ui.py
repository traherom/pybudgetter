import glob
import os.path

from PyQt5 import uic

def compile_all():
    print('Compiling all UI files...')
    
    relative = os.path.join(os.path.dirname(__file__), '..')
    ui_path = os.path.join(relative, 'resources/uixml/*.ui')
    ui_path = os.path.normpath(ui_path)
    out_path = os.path.normpath(os.path.join(relative, 'ui'))
    print(ui_path, out_path)
    
    for uif in glob.iglob(ui_path):
        base = os.path.basename(uif).split('.')[0]
        out_py = os.path.join(out_path, 'ui_{}.py'.format(base))
        
        print('\t{} -> {}'.format(uif, out_py))

        with open(out_py, 'w') as out:
            uic.compileUi(uif, out, from_imports=True)

    print('Done!')
        
if __name__ == '__main__':
    compile_all()
    
