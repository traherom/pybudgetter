from PyQt5.QtCore import Qt, QEvent
from PyQt5.QtWidgets import (QApplication, QStyledItemDelegate,
                             QPushButton, QStyle, QStyleOptionButton)
from PyQt5.QtGui import QBrush, QPalette, QPen

class RemoveItemDelegate(QStyledItemDelegate):
    def __init__(self, evthandler, parent=None):
        """Event handler must have take whatever type is in the column
        this delegate is used on"""
        super(RemoveItemDelegate, self).__init__(parent)
        self.__over = False
        self.__depressed = False

        self.__handler = evthandler

    def paint(self, painter, opt, index):
        # Don't show anything for the 'add' row, which has an id < 0
        if index.data() < 0:
            return
            
        if opt.state & QStyle.State_Selected:
            painter.setPen(QPen(Qt.NoPen))
            if opt.state & QStyle.State_Active:
                painter.setBrush(QBrush(QPalette().highlight()))
            else:
                painter.setBrush(QBrush(QPalette().color(QPalette.Inactive, QPalette.Highlight)))

        rm_btn_style = QStyleOptionButton()
        rm_btn_style.state |= QStyle.State_Enabled | QStyle.State_Raised
        rm_btn_style.rect = opt.rect
        rm_btn_style.text = 'x'

        # Couple more style options based on the mouse
        if self.__depressed == index:
            rm_btn_style.state |= QStyle.State_Sunken
        elif self.__over == index:
            rm_btn_style.state |= QStyle.State_MouseOver

        QApplication.style().drawControl(QStyle.CE_PushButton,
                                         rm_btn_style,
                                         painter)

    def editorEvent(self, event, model, opt, index):
        """Handle things like button highlights when hovered,
        button pressed appearance, and the actual signal
        when the button is pressed"""
        if event.type() == QEvent.Enter:
            # Mouse is over the button
            self.__over = index
            print('over')
            return True
        elif event.type() == QEvent.Leave:
            self.__over = None
            self.__depressed = None
            print('leave')
            return True
        elif event.type() == QEvent.MouseButtonPress:
            self.__depressed = index
            print('pressed')
            return True
        elif event.type() == QEvent.MouseButtonRelease:
            # Don't activate if the mouse was pressed before
            # entering the button area
            print('released')
            if self.__depressed == index:
                # TODO emit event
                print('DELETE')
                self.__handler(index.data())

            # Well we're definitely not pressed any more
            self.__pressed = None
            return True
        else:
            return False

    def createEditor(self, parent, option, index):
        # Prevent an editor from being spawned
        return None

    def setEditorData(self, editor, index):
        pass

    def setModelData(self, editor, model, index):
        pass

    def updateEditorGeometry(self, editor, options, index):
        editor.setGeometry(options.rect)
