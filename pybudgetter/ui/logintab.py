from PyQt5.QtWidgets import QWidget, QLineEdit, QDataWidgetMapper, QMessageBox
from PyQt5.QtCore import Qt, QFile, QIODevice, pyqtSlot
from PyQt5 import uic

from ..budget.helpers import logins

class LoginTabWidget(QWidget):
    def __init__(self, budget, parent=None):
        super(LoginTabWidget, self).__init__(parent)

        self.__budget = budget
        self.__creating_login = False

        # Load UI XML
        file = QFile(':logintab.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open login tab UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        if self.budget.is_open():
            self.enable_tab()
        else:
            self.disable_tab()

        # Attach to budget
        self.budget.opened.connect(self.enable_tab)
        self.budget.closed.connect(self.clear_details)
        self.budget.closed.connect(self.disable_tab)

        self.__answer_inputs = list()
        self.__login_model = self.budget.create_login_model()
        self.ui.loginSelectCombo.setModel(self.__login_model)

        self.__login_settings_model = self.budget.create_login_settings_model()
        self.__login_settings_mapper = QDataWidgetMapper()
        self.__login_settings_mapper.setModel(self.__login_settings_model)
        self.__login_settings_mapper.setSubmitPolicy(QDataWidgetMapper.ManualSubmit)
        self.__login_settings_mapper.setOrientation(Qt.Vertical)

        self.budget.logins_changed.connect(self.on_logins_changed)

        # Accounts (under logins)
        self.__accounts_model = self.budget.create_accounts_model()
        self.ui.loginAccountsList.setModel(self.__accounts_model)

        self.ui.loginAccountsList.setModelColumn(self.__accounts_model.name_col)

        # Populate plugin list
        plugins = self.budget.scanner.get_plugin_list()
        self.ui.loginPluginsCombo.clear()
        self.ui.loginPluginsCombo.addItem('Manual')
        for plugin in sorted(plugins):
            self.ui.loginPluginsCombo.addItem(plugin)

        # Make sure the details are enabled/disabled as appropriate
        self.set_details_to_selection()

    @property
    def budget(self):
        return self.__budget

    def enable_tab(self, enabled=True):
        """Enables interaction with tab"""
        self.ui.loginCreateBtn.setEnabled(enabled)
        self.ui.loginSelectCombo.setEnabled(enabled)

    def disable_tab(self, disabled=True):
        """Disable interaction with tab"""
        self.enable_tab(not disabled)

    def clear_details(self):
        """Remove all plugin-specific widgets"""
        self.__login_id = None
        self.__login_settings_mapper.clearMapping()

        for answer in self.__answer_inputs:
            label = self.ui.questionsLayout.labelForField(answer)
            label.deleteLater()
            answer.deleteLater()

        self.__answer_inputs = list()

        self.__login_settings_model.clear_login_id()
        self.__accounts_model.clear_login_id()

        # Disable universal elements
        self.ui.loginLastErrorLbl.clear()

        self.ui.loginPluginsCombo.setEnabled(False)
        self.ui.loginSaveBtn.setEnabled(False)
        self.ui.loginRevertBtn.setEnabled(False)
        self.ui.loginAccountsList.setEnabled(False)
        self.ui.deleteBtn.setEnabled(False)
        self.ui.resetBtn.setEnabled(False)

    def set_details_to_selection(self):
        """
        Finds the login ID of the current login combo box selection and
        syncs the disabled details to that. If nothing is selected or there
        is nothing in the combo, clears the tab
        """
        selection = self.ui.loginSelectCombo.currentIndex()
        if selection != -1:
            id_index = self.__login_model.id_col
            login_id = self.__login_model.index(selection, id_index).data()
            self.set_details(login_id)
        else:
            self.clear_details()

    def set_details(self, login_id):
        """
        Sets the current login to the given ID and displays current settings for the login
        """
        self.clear_details()
        if not self.budget.is_open():
            return

        self.__login_id = login_id

        # Show accounts under this login
        self.__accounts_model.set_login_id(login_id)

        # Select the correct plugin
        self.ui.loginPluginsCombo.setCurrentText(self.budget.get_login_plugin(login_id))

        # Add a row for each user setting for this login
        self.__login_settings_model.set_login_id(login_id)

        input_to_select = None
        settings = self.budget.get_all_login_settings(login_id)
        self.__answer_inputs = list()

        for i in range(len(settings)):
            setting = settings[i]

            #print('Setting {}: {}'.format(setting.get_key(), setting.get_value()))

            # Special consideration for the error message
            if not setting.is_user_setting():
                if setting.get_key() == logins.BudgetLoginSetting.ERROR_KEY:
                    err = setting.get_value()
                    if err:
                        self.ui.loginLastErrorLbl.setText(err)
                    else:
                        self.ui.loginLastErrorLbl.clear()

                continue

            answer = QLineEdit()
            self.__answer_inputs.append(answer)
            # put on list?
            self.__login_settings_mapper.addMapping(answer, i)

            # Styling
            if setting.is_needed():
                answer.setStyleSheet('QLineEdit { border: 1px solid red; }')

                if input_to_select is None:
                    input_to_select = answer

            if setting.get_key() == logins.BudgetLoginSetting.PASSWORD_KEY:
                answer.setEchoMode(QLineEdit.PasswordEchoOnEdit)

            self.ui.questionsLayout.insertRow(i + 1, setting.get_prompt(), answer)

        # Populate data in widgets
        self.__login_settings_mapper.setCurrentIndex(self.__login_settings_model.value_col)

        if input_to_select is not None:
            input_to_select.selectAll()

        self.ui.loginPluginsCombo.setEnabled(False)
        self.ui.loginSaveBtn.setEnabled(True)
        self.ui.loginRevertBtn.setEnabled(True)
        self.ui.loginAccountsList.setEnabled(True)
        self.ui.deleteBtn.setEnabled(True)
        self.ui.resetBtn.setEnabled(True)

    def __reselect_current_login(self):
        for row in range(self.__login_model.rowCount()):
            id = self.__login_model.index(row, self.__login_model.id_col).data()
            if id == self.__login_id:
                self.ui.loginSelectCombo.setCurrentIndex(row)

            self.set_details_to_selection()

    @pyqtSlot()
    def on_logins_changed(self):
        # If there is not currently a login selected, get the top one
        if self.__login_id is not None:
            self.__reselect_current_login()
        else:
            self.ui.loginSelectCombo.setCurrentIndex(0)

        self.set_details_to_selection()

    @pyqtSlot(int)
    def on_loginSelectCombo_activated(self, i):
        self.__creating_login = False
        self.set_details_to_selection()

    @pyqtSlot(bool)
    def on_loginCreateBtn_clicked(self):
        self.__creating_login = True
        self.ui.loginSelectCombo.setCurrentIndex(-1)
        self.clear_details()

        self.ui.loginPluginsCombo.setEnabled(True)
        self.ui.loginSaveBtn.setEnabled(True)
        self.ui.loginRevertBtn.setEnabled(False)
        self.ui.loginAccountsList.setEnabled(False)
        self.ui.deleteBtn.setEnabled(False)
        self.ui.resetBtn.setEnabled(False)

    @pyqtSlot(bool)
    def on_loginSaveBtn_clicked(self):
        if self.__creating_login:
            # Create new login
            plugin = self.ui.loginPluginsCombo.currentText()
            self.__login_id = self.budget.create_login(plugin)

            #  Select login in combo box
            self.__creating_login = False
            self.__reselect_current_login()
        else:
            # Save changes
            self.__login_settings_mapper.submit()
            self.__login_settings_model.reinit_model()
            self.set_details(self.__login_id)

        # Start a scan
        self.budget.scanner.scan_login(self.__login_id)

    @pyqtSlot(bool)
    def on_loginRevertBtn_clicked(self):
        """Reverts any changes to the current login"""
        if self.__creating_login:
            self.__creating_login = False
            self.clear_details()
        else:
            self.__login_settings_mapper.revert()

    @pyqtSlot()
    def on_deleteBtn_clicked(self):
        """Removes the current login entirely"""
        answer = QMessageBox.question(self, 'Delete login?',
                    'Deleting this login will remove all transactions '
                    'under it and require you to renter all the login details '
                    'to use it again.\n\nDo you want to continue?')

        if answer == QMessageBox.Yes:
            id = self.__login_id
            self.__login_id = None
            self.budget.remove_login(id)

    @pyqtSlot()
    def on_resetBtn_clicked(self):
        """Removes all accounts and transactions from the current login"""
        answer = QMessageBox.question(self, 'Reset login?',
                    'Resetting this login will remove all transactions '
                    'under it.\n\nDo you want to continue?')

        if answer == QMessageBox.Yes:
            self.budget.reset_login(self.__login_id)
