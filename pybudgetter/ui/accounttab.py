from pprint import pprint

from PyQt5.QtWidgets import QWidget, QHeaderView, QMenu, QMessageBox
from PyQt5.QtCore import Qt, QPoint, pyqtSlot, QFile, QIODevice
from PyQt5 import uic

from . import proxymodels
from ..budget import utilities, filters, helpers


class AccountTabWidget(QWidget):
    def __init__(self, budget, parent=None):
        super(AccountTabWidget, self).__init__(parent)

        self.budget = budget

        # Load UI XML
        file = QFile(':accounttab.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open account tab UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        if self.budget.is_open():
            self.setEnabled()
        else:
            self.setDisabled()

        # Attach to budget
        self.budget.opened.connect(self.setEnabled)
        self.budget.closed.connect(self.setDisabled)

        self.accounts_model = proxymodels.AccountsProxyModel(self.budget)
        self.ui.accountView.setModel(self.accounts_model)
        self.ui.accountView.verticalHeader().hide()
        self.ui.accountView.setSortingEnabled(True)

        # Manage sizing
        headers = self.ui.accountView.horizontalHeader()
        headers.hideSection(self.accounts_model.id_col)
        headers.setStretchLastSection(False)
        headers.setSectionResizeMode(QHeaderView.ResizeToContents)
        headers.setSectionResizeMode(self.accounts_model.name_col, QHeaderView.Stretch)

        # Add menu to allow removal of categories and creation of rules
        self.__menu = QMenu()

        self.__menu.addAction('Add').triggered.connect(self.on_account_add)
        self.__menu.addSeparator()

        self.__clear_action = self.__menu.addAction('Clear all transactions...')
        self.__clear_action.triggered.connect(self.on_account_clear)

        self.__menu.addSeparator()
        self.__remove_action = self.__menu.addAction('Remove account...')
        self.__remove_action.triggered.connect(self.on_account_remove)

        # self.__import_action = self.__menu.addAction('Import transactions...')
        #self.__import_action.triggered.connect(self.on_import)

        self.ui.accountView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui.accountView.customContextMenuRequested.connect(self.on_show_context_menu)

    def setEnabled(self, enabled=True):
        self.ui.accountView.setEnabled(enabled)

    def setDisabled(self, disabled=True):
        self.setEnabled(not disabled)

    @pyqtSlot('QPoint')
    def on_show_context_menu(self, pos):
        if self.ui.accountView.selectedIndexes():
            self.__remove_action.setEnabled(True)
            self.__clear_action.setEnabled(True)
            # self.__import_action.setEnabled(True)
        else:
            self.__remove_action.setEnabled(False)
            self.__clear_action.setEnabled(False)
            # self.__import_action.setEnabled(False)

        globalPos = self.ui.accountView.mapToGlobal(pos)
        menuPos = QPoint(globalPos.x(), globalPos.y() + self.__menu.sizeHint().height() / 2)
        self.__menu.popup(menuPos)

    @pyqtSlot('QAction')
    def on_account_clear(self, action):
        ids = self.__selected_to_ids()
        cnt = len(ids)

        if cnt == 1:
            answer = QMessageBox.question(self, 'Clear account?',
                                          'Are you sure you want to remove all transactions from this account?')
        elif cnt > 1:
            answer = QMessageBox.question(self, 'Clear accounts?',
                                          'Are you sure you want to remove all transactions from these {} accounts?'.format(cnt))
        else:
            # Nothing was selected?
            return

        if answer == QMessageBox.Yes:
            for id in ids:
                self.budget.accounts[id].clear_transactions()

    @pyqtSlot('QAction')
    def on_account_remove(self, action):
        # Cache IDs to be removed, because the rows change once we start removing
        to_be_removed = self.__selected_to_ids()

        cnt = len(to_be_removed)

        if cnt == 1:
            answer = QMessageBox.question(self, 'Remove account?',
                                          'Are you sure you want to remove this account? All ' +
                                          'transactions assigned to it will be removed.')
        elif cnt > 1:
            answer = QMessageBox.question(self, 'Remove accounts?',
                                          'Are you sure you want to remove these {} accounts? All '.format(cnt) +
                                          'transactions associated with these accounts will be removed.')
        else:
            # Nothing was selected?
            return

        if answer == QMessageBox.Yes:
            for id in to_be_removed:
                self.budget.accounts[id].remove()

    @pyqtSlot('QAction')
    def on_account_add(self, action):
        # Add new account
        row = 0
        selected = self.ui.accountView.selectedIndexes()
        if selected:
            row = selected[0].row()

        self.accounts_model.insertRow(row)

    def __selected_to_ids(self):
        selected = self.ui.accountView.selectedIndexes()
        ids = list()
        for item in selected:
            ids.append(item.sibling(item.row(), self.accounts_model.id_col).data())

        # Remove duplicates. Needs to be done because selectedIndexes()
        # returns an index for each cell in the rows, not just one per row
        return list(set(ids))

    def __set_lbl_color(self, lbl, amt):
        """Sets the given label's stylesheet according to the given amount
        (< 0=red, >0=green)"""
        if amt < 0:
            lbl.setStyleSheet('QLabel { color: red; }')
        else:
            lbl.setStyleSheet('QLabel { color: green; }')
