from PyQt5.QtWidgets import QWidget, QHeaderView, QMenu, QMessageBox
from PyQt5.QtCore import Qt, QFile, QIODevice, QDate, QPoint, pyqtSlot
from PyQt5 import uic

from . import proxymodels
from ..budget import utilities, filters, helpers

class CategoryTabWidget(QWidget):
    def __init__(self, budget, parent=None):
        super(CategoryTabWidget, self).__init__(parent)

        self.__budget = budget

        # Load UI XML
        file = QFile(':categorytab.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open category tab UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        if self.budget.is_open():
            self.enable_tab()
        else:
            self.disable_tab()

        # Attach to budget
        self.budget.opened.connect(self.enable_tab)
        self.budget.closed.connect(self.disable_tab)

        self.__category_model = proxymodels.CategoryProxyModel(self.budget)
        self.ui.categoryView.setModel(self.__category_model)
        self.ui.categoryView.verticalHeader().hide()
        self.ui.categoryView.setSortingEnabled(True)

        self.budget.categories.reset.connect(self.on_categories_updated)
        self.budget.categories.changed.connect(self.on_categories_updated)
        self.budget.categories.added.connect(self.on_categories_updated)
        self.budget.categories.removed.connect(self.on_categories_updated)

        self.budget.transactions.reset.connect(self.on_transactions_updated)
        self.budget.transactions.changed.connect(self.on_transactions_updated)
        self.budget.transactions.added.connect(self.on_transactions_updated)
        self.budget.transactions.removed.connect(self.on_transactions_updated)

        # Manage sizing
        headers = self.ui.categoryView.horizontalHeader()
        headers.hideSection(self.__category_model.id_col)
        headers.setStretchLastSection(False)
        headers.setSectionResizeMode(QHeaderView.ResizeToContents)
        headers.setSectionResizeMode(self.__category_model.desc_col, QHeaderView.Stretch)

        allocated_width = headers.sectionSize(self.__category_model.amount_col)
        self.ui.incomeAllocatedLbl.setMinimumWidth(allocated_width)
        self.ui.incomeAllocatedLbl.setMaximumWidth(allocated_width)

        used_width = headers.sectionSize(self.__category_model.used_col)
        self.ui.incomeUsedLbl.setMinimumWidth(used_width)
        self.ui.incomeUsedLbl.setMaximumWidth(used_width)

        # Prevent to date going past today
        self.ui.toDate.setMaximumDate(QDate.currentDate())

        # Add menu to allow removal of categories and creation of rules
        self.__menu = QMenu()

        self.__menu.addAction('Add').triggered.connect(self.on_category_add)
        self.__remove_action = self.__menu.addAction('Remove...')
        self.__remove_action.triggered.connect(self.on_category_remove)

        self.ui.categoryView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui.categoryView.customContextMenuRequested.connect(self.on_show_context_menu)

    @property
    def budget(self):
        return self.__budget

    def enable_tab(self, enabled=True):
        self.ui.categoryView.setEnabled(enabled)
        self.ui.toDate.setEnabled(enabled)
        self.ui.fromDate.setEnabled(enabled)

    def disable_tab(self, disabled=True):
        self.enable_tab(not disabled)

    @pyqtSlot('QPoint')
    def on_show_context_menu(self, pos):
        if self.ui.categoryView.selectedIndexes():
            self.__remove_action.setEnabled(True)
        else:
            self.__remove_action.setEnabled(False)

        globalPos = self.ui.categoryView.mapToGlobal(pos)
        menuPos = QPoint(globalPos.x(), globalPos.y() + self.__menu.sizeHint().height() / 2)
        self.__menu.popup(menuPos)

    @pyqtSlot('QAction')
    def on_category_remove(self, action):
        # Cache IDs to be removed, because the rows change once we start removing
        to_be_removed = self.__selected_to_ids()

        cnt = len(to_be_removed)

        if cnt == 1:
            answer = QMessageBox.question(self, 'Remove category?',
                    'Are you sure you want to remove this category? All ' +
                    'transactions assigned to it will be uncategorized.')
        elif cnt > 1:
            answer = QMessageBox.question(self, 'Remove categories?',
                    'Are you sure you want to remove these {} categories? All '.format(cnt) +
                    'transactions assigned to them will be uncategorized.')
        else:
            # Nothing was selected?
            return

        if answer == QMessageBox.Yes:
            cats = [self.budget.categories[i] for i in to_be_removed]
            for cat in cats:
                cat.remove()

    @pyqtSlot('QAction')
    def on_category_add(self, action):
        # Add new category list
        row = 0
        selected = self.ui.categoryView.selectedIndexes()
        if selected:
            row = selected[0].row()

        self.__category_model.insertRow(row)

    @pyqtSlot()
    @pyqtSlot(object)
    def on_categories_updated(self, cat=None):
        """Totals up income and expenses from the categories and updates
        the appropriate labels"""
        # Allocated columns
        income = self.budget.categories.get_income_total()
        expenses = self.budget.categories.get_expenses_total()

        self.ui.incomeAllocatedLbl.setText(utilities.string_dollars(income))
        self.__set_lbl_color(self.ui.incomeAllocatedLbl, income)

        self.ui.expensesAllocatedLbl.setText(utilities.string_dollars(expenses))
        self.__set_lbl_color(self.ui.expensesAllocatedLbl, expenses)

        total = income + expenses
        self.ui.totalAllocatedLbl.setText(utilities.string_dollars(total))
        self.__set_lbl_color(self.ui.totalAllocatedLbl, total)

        # Changed categories that went from income to expense or vice versa
        # would mean the used totals need updating too
        self.on_transactions_updated()

    @pyqtSlot()
    @pyqtSlot(object)
    def on_transactions_updated(self, trans=None):
        """Totals up income and expenses from transactions for each category and updates
        the appropriate labels"""
        # Used columns
        income = self.budget.transactions.income_used_total(self.ui.fromDate.date(), self.ui.toDate.date())
        expenses = self.budget.transactions.expenses_used_total(self.ui.fromDate.date(), self.ui.toDate.date())

        self.ui.incomeUsedLbl.setText(utilities.string_dollars(income))
        self.__set_lbl_color(self.ui.incomeUsedLbl, income)

        self.ui.expensesUsedLbl.setText(utilities.string_dollars(expenses))
        self.__set_lbl_color(self.ui.expensesUsedLbl, expenses)

        total = income + expenses
        self.ui.totalUsedLbl.setText(utilities.string_dollars(total))
        self.__set_lbl_color(self.ui.totalUsedLbl, total)

    def __selected_to_ids(self):
        selected = self.ui.categoryView.selectedIndexes()
        ids = list()
        for item in selected:
            ids.append(item.sibling(item.row(), self.__category_model.id_col).data())

        # Remove duplicates. Needs to be done because selectedIndexes()
        # returns an index for each cell in the rows, not just one per row
        return list(set(ids))

    def __set_lbl_color(self, lbl, amt):
        """Sets the given label's stylesheet according to the given amount
        (< 0=red, >0=green)"""
        if amt < 0:
            lbl.setStyleSheet('QLabel { color: red; }')
        else:
            lbl.setStyleSheet('QLabel { color: green; }')

    @pyqtSlot('QDate')
    def on_toDate_dateChanged(self, date):
        # Don't allow from date to be set further than this date
        self.ui.fromDate.setMaximumDate(date)

        self.__category_model.set_to_date(date)
        self.on_transactions_updated()

    @pyqtSlot('QDate')
    def on_fromDate_dateChanged(self, date):
        # Don't allow to date to be set earlier than this date
        self.ui.toDate.setMinimumDate(date)

        self.__category_model.set_from_date(date)
        self.on_transactions_updated()
