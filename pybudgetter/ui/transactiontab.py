from pprint import pprint

from PyQt5.QtWidgets import QWidget, QHeaderView, QMenu, QPushButton
from PyQt5.QtCore import Qt, QFile, QIODevice, QPoint, pyqtSlot, QDate
from PyQt5 import uic

from . import proxymodels, filtereditor, importwindow

class TransactionTabWidget(QWidget):
    def __init__(self, budget, parent=None):
        super(TransactionTabWidget, self).__init__(parent)

        self.__budget = budget

        # Load UI XML
        file = QFile(':transactiontab.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open transaction tab UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        self.ui.filter_widget = filtereditor.FilterEditor(self.budget, self)
        apply_btn = QPushButton('Apply')
        apply_btn.clicked.connect(self.on_filter_apply_clicked)
        self.ui.filter_widget.add_extra_button(apply_btn)

        if self.budget.is_open():
            self.enable_tab()
        else:
            self.disable_tab()

        self.import_dialog = importwindow.ImportWindow(self, self.budget)

        # Attach to budget
        self.budget.opened.connect(self.enable_tab)
        self.budget.closed.connect(self.disable_tab)

        self.budget.categories.reset.connect(self.on_categories_updated)
        self.budget.categories.added.connect(self.on_categories_updated)
        self.budget.categories.removed.connect(self.on_categories_updated)
        self.budget.categories.changed.connect(self.on_categories_updated)

        self.__transactions_model = proxymodels.TransactionProxyModel(self.budget)
        self.ui.transactionsView.setModel(self.__transactions_model)
        self.ui.transactionsView.verticalHeader().hide()

        # Set initial filtering
        self.ui.toDate.setMaximumDate(QDate.currentDate())
        self.ui.advancedFilterBox.layout().insertWidget(0, self.ui.filter_widget)
        self.ui.advancedFilterBox.hide()

        # Manage sizing
        headers = self.ui.transactionsView.horizontalHeader()
        headers.hideSection(self.__transactions_model.id_col)
        headers.hideSection(self.__transactions_model.cat_col)
        headers.setStretchLastSection(False)
        headers.setSectionResizeMode(QHeaderView.ResizeToContents)
        headers.setSectionResizeMode(self.__transactions_model.desc_col, QHeaderView.Stretch)
        headers.hideSection(self.__transactions_model.account_id_col)

        #self.ui.newestBtnBalance.setMinimumSize(self.ui.newestBtn.sizeHint())

        # Add menu to allow categorization of transactions
        self.__menu = QMenu()
        self.__cat_menu = self.__menu.addMenu('Categorize')
        self.__cat_menu.triggered.connect(self.on_menu_categorize)

        self.ui.transactionsView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui.transactionsView.customContextMenuRequested.connect(self.on_show_context_menu)

    @property
    def budget(self):
        return self.__budget

    def enable_tab(self, enabled=True):
        """Enables interaction with tab"""
        self.ui.advancedFilterToggleBtn.setEnabled(enabled)
        self.ui.filter_widget.setEnabled(enabled)

        if not self.ui.advancedFilterToggleBtn.isChecked():
            self.ui.filterTxt.setEnabled(enabled)
            self.ui.onlyUncatChkBox.setEnabled(enabled)

        self.ui.importBtn.setEnabled(enabled)
        self.ui.transactionsView.setEnabled(enabled)
        self.ui.toDate.setEnabled(enabled)
        self.ui.fromDate.setEnabled(enabled)
        self.ui.olderBtn.setEnabled(enabled)

        self.__update_date_buttons()

    def disable_tab(self, disabled=True):
        """Disable interaction with tab"""
        self.enable_tab(not disabled)

    def __update_date_buttons(self):
        """Ensures that the newer and newest buttons are enabled
        /disabled as appropriate"""
        # Disable buttons if needed
        if self.ui.toDate.date() >= self.ui.toDate.maximumDate():
            self.ui.newestBtn.setEnabled(False)
            self.ui.newerBtn.setEnabled(False)
        else:
            self.ui.newestBtn.setEnabled(True)
            self.ui.newerBtn.setEnabled(True)

    @pyqtSlot('QPoint')
    def on_show_context_menu(self, pos):
        if self.ui.transactionsView.selectedIndexes():
            self.__cat_menu.setEnabled(True)
        else:
            self.__cat_menu.setEnabled(False)

        globalPos = self.ui.transactionsView.mapToGlobal(pos)
        menuPos = QPoint(globalPos.x(), globalPos.y() + self.__menu.sizeHint().height() / 2)
        self.__menu.popup(menuPos)

    @pyqtSlot()
    @pyqtSlot(object)
    def on_categories_updated(self, item=None):
        """Updates the categories menu"""
        self.__cat_menu.clear()
        self.__cat_menu.addAction('Remove Category')
        self.__cat_menu.addSeparator()
        self.__cat_menu.addAction('Apply Rules')
        self.__cat_menu.addSeparator()

        if self.budget.categories:
            for cat in self.budget.categories:
                self.__cat_menu.addAction(cat.name)
        else:
            self.__cat_menu.addAction('None').setDisabled(True)

    @pyqtSlot('QAction')
    def on_menu_categorize(self, action):
        """Assign the category to the selected transactions"""
        selected = self.ui.transactionsView.selectedIndexes()
        trans_ids = list()
        for item in selected:
            trans_ids.append(item.sibling(item.row(), self.__transactions_model.id_col).data())

        # What is the selected category's ID?
        if action.text() == 'Apply Rules':
            for id in trans_ids:
                trans = self.budget.transactions[id]
                trans.apply_rules()
                trans.save()
        else:
            if action.text() != 'Remove Category':
                cat_id = self.budget.categories.find(name=action.text()).id
            else:
                cat_id = None

            for id in trans_ids:
                trans = self.budget.transactions[id]
                trans.category_id = cat_id
                trans.save()

    @pyqtSlot(bool)
    def on_onlyUncatChkBox_toggled(self, show_only_uncat):
        self.__transactions_model.show_uncategorized_only(show_only_uncat)

    @pyqtSlot(str)
    def on_filterTxt_textChanged(self, txt):
        self.__transactions_model.setFilterRegExp(txt)

    @pyqtSlot()
    def on_importBtn_clicked(self):
        """Lets the user import a file with transactions"""
        self.import_dialog.start_import()

    def go_to_newest(self):
        # To date must be set first to allow from date to be set ahead
        self.ui.toDate.setDate(self.__end_of_month())
        self.ui.fromDate.setDate(self.__begin_of_month())

    def __begin_of_month(self):
        today = QDate.currentDate()
        return QDate(today.year(), today.month(), 1)

    def __end_of_month(self):
        beg_of_mon = self.__begin_of_month()
        return beg_of_mon.addMonths(1).addDays(-1)

    @pyqtSlot(bool)
    def on_advancedFilterToggleBtn_toggled(self, checked):
        if checked:
            # Use advanced filter
            self.use_advanced_filter()
        else:
            # Use basic filter
            self.use_basic_filter()

    def use_basic_filter(self):
        self.__transactions_model.clear_filters()
        self.__transactions_model.setFilterRegExp(self.ui.filterTxt.text())
        if self.ui.onlyUncatChkBox.isChecked():
            self.__transactions_model.show_uncategorized_only()

        self.ui.onlyUncatChkBox.setEnabled(True)
        self.ui.filterTxt.setEnabled(True)
        self.ui.advancedFilterBox.hide()

    def use_advanced_filter(self):
        self.__transactions_model.clear_filters()
        self.__transactions_model.setFilterRegExp('')

        self.ui.onlyUncatChkBox.setEnabled(False)
        self.ui.filterTxt.setEnabled(False)
        self.ui.advancedFilterBox.show()

    @pyqtSlot()
    def on_filter_apply_clicked(self):
        self.__transactions_model.filters = self.ui.filter_widget.filters

    @pyqtSlot()
    def on_newestBtn_clicked(self):
        self.go_to_newest()

    @pyqtSlot()
    def on_newerBtn_clicked(self):
        # Move forward a month, putting to date at the END of the next month
        to = self.ui.toDate.date().addMonths(2)
        to.setDate(to.year(), to.month(), 1)
        to = to.addDays(-1)

        fromDate = QDate(to.year(), to.month(), 1)

        # Set to date first so that fromDate is allowed to go all the forward
        # to where we want (otherwise maximum will limit it)
        self.ui.toDate.setDate(to)
        self.ui.fromDate.setDate(fromDate)

    @pyqtSlot()
    def on_olderBtn_clicked(self):
        # Move back a month, putting to date at the END of the previous month
        to = self.ui.toDate.date().addMonths(1)
        to.setDate(to.year(), to.month(), 1)
        to = to.addDays(-1).addMonths(-1)

        fromDate = QDate(to.year(), to.month(), 1)

        # Set from date first so that toDate is allowed to go all the back
        # to where we want (otherwise maximum will limit it)
        self.ui.fromDate.setDate(fromDate)
        self.ui.toDate.setDate(to)

    @pyqtSlot('QDate')
    def on_toDate_dateChanged(self, date):
        # Don't allow from date to be set further than this date
        self.ui.fromDate.setMaximumDate(date)

        self.__transactions_model.set_to_date(date)
        self.__update_date_buttons()

    @pyqtSlot('QDate')
    def on_fromDate_dateChanged(self, date):
        # Don't allow to date to be set earlier than this date
        self.ui.toDate.setMinimumDate(date)

        self.__transactions_model.set_from_date(date)
