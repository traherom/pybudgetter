from pprint import pprint

from PyQt5.QtWidgets import QDialog, QPushButton, QHeaderView
from PyQt5.QtCore import QFile, QIODevice, pyqtSlot
from PyQt5 import uic

from . import filtereditor, proxymodels
from ..budget.helpers import *

class RuleEditorWindow(QDialog):
    def __init__(self, budget, parent=None):
        super(RuleEditorWindow, self).__init__(parent)

        self.__budget = budget
        self.__rule = None

        # UI setup
        file = QFile(':ruleseditor.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open rule editor UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        #self.setWindowFlags(Qt.Sheet)

        self.ui.filter_widget = filtereditor.FilterEditor(self.budget, self)
        test_btn = QPushButton('Test')
        test_btn.clicked.connect(self.on_filter_test_clicked)
        self.ui.filter_widget.add_extra_button(test_btn)

        self.ui.filterGroupBox.layout().insertWidget(0, self.ui.filter_widget)

        # Attach to budget
        self.budget.closed.connect(self.on_budget_closed)

        self.__transactions_model = proxymodels.TransactionProxyModel(self.budget)
        self.ui.transactionView.setModel(self.__transactions_model)
        self.ui.transactionView.verticalHeader().hide()

        self.__cat_model = proxymodels.CategoryProxyModel(self.budget)
        self.ui.categoryBox.setModel(self.__cat_model)
        self.ui.categoryBox.setModelColumn(self.__cat_model.desc_col)

        # Manage sizing
        headers = self.ui.transactionView.horizontalHeader()
        headers.hideSection(self.__transactions_model.id_col)
        headers.hideSection(self.__transactions_model.cat_col)
        headers.setStretchLastSection(False)
        headers.setSectionResizeMode(QHeaderView.ResizeToContents)
        headers.setSectionResizeMode(self.__transactions_model.desc_col, QHeaderView.Stretch)
        headers.hideSection(self.__transactions_model.account_id_col)

    @property
    def budget(self):
        return self.__budget

    def edit_rule(self, rule):
        """Configures the window to edit the given rule"""
        if type(rule) == int:
            rule = self.budget.rules[rule]

        self.__rule = rule
        self.__update_ui()
        self.show()

    def create_rule(self):
        self.__rule = rules.RuleItem.from_values(self.budget,
                                                name='New Rule',
                                                category_id=None)
        self.__update_ui()
        self.show()

    def __update_ui(self):
        if self.__rule is None:
            # Blank all
            self.ui.categoryBox.clear()
            self.ui.ruleNameTxt.clear()
            self.ui.filter_widget.clear()
        else:
            # Results/action
            for i in range(self.__cat_model.rowCount()):
                if self.__rule.category_id == self.__cat_model.index(i,
                        self.__cat_model.id_col).data():
                    self.ui.categoryBox.setCurrentIndex(i)
                    break

            # Filters
            self.ui.filter_widget.filters = self.__rule.filters
            self.ui.ruleNameTxt.setText(self.__rule.name)

        self.test_filter()

    def test_filter(self):
        self.__transactions_model.filters = self.ui.filter_widget.filters

    @pyqtSlot()
    def on_filter_test_clicked(self):
        """Applies the current filter to the transaction view"""
        self.test_filter()

    @pyqtSlot()
    def on_saveBtn_clicked(self):
        """Save changes to rule and hide"""
        # Save
        self.__rule.name = self.ui.ruleNameTxt.text()
        category_id = self.__cat_model.index(self.ui.categoryBox.currentIndex(),
                                        self.__cat_model.id_col).data()
        self.__rule.category_id = category_id
        self.__rule.filters = self.ui.filter_widget.filters
        self.__rule.save()

        # And close
        self.__rule = None
        self.hide()

    @pyqtSlot()
    def on_cancelBtn_clicked(self):
        """Discard changes and hide"""
        self.__rule = None
        self.hide()

    @pyqtSlot()
    def on_budget_closed(self):
        """If the budget is closed, hide this window"""
        self.hide()
