from PyQt5.QtWidgets import QDialog, QHeaderView, QTableWidgetItem, QComboBox, QFileDialog
from PyQt5.QtCore import Qt, QFile, QIODevice
from PyQt5 import uic

from ..budget import importer

class ImportWindow(QDialog):
    def __init__(self, parent, budget):
        super(ImportWindow, self).__init__(parent)

        self.budget = budget

        # Load UI
        file = QFile(':importerwindow.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open importer window UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        self.setWindowFlags(Qt.Sheet)

        # Beautify the table
        headers = self.importMatchTable.horizontalHeader()
        headers.setSectionResizeMode(QHeaderView.ResizeToContents)
        headers.setStretchLastSection(True)

        self.clear()

    def import_from(self, path):
        """Begins the import process for the given path"""
        self.clear()
        self.show()

        self.ui.importPathTxt.setText(path)

        # Read in file and populate table based on it
        self.imported = importer.from_file(self.budget, path)

        rows = len(self.imported.accounts)
        self.importMatchTable.setRowCount(rows)
        for r in range(rows):
            acc = self.imported.accounts[r]

            self.importMatchTable.setItem(r, 0, QTableWidgetItem(acc.routing_number))
            self.importMatchTable.setItem(r, 1, QTableWidgetItem(acc.account_number))
            self.importMatchTable.setItem(r, 2, QTableWidgetItem(acc.type))
            self.importMatchTable.setItem(r, 3, QTableWidgetItem(str(len(acc.transactions))))

            # Final column allows selection of local account to import to
            # Attempt to select the local account that best matches
            sel = QComboBox()
            sel.addItem('Skip')
            sel.insertSeparator(1)
            for local_acc in self.budget.accounts:
                sel.addItem(local_acc.name, local_acc.id)

            #print('looking for match for {}:{}'.format(acc.routing_number, acc.account_number))
            match = self.budget.accounts.find(routing_number=acc.routing_number,
                                              account_number=acc.account_number)
            if match is None:
                #print('looking for match for {}'.format(acc.account_number))
                match = self.budget.accounts.find(account_number=acc.account_number)
            if match is not None:
                #print('selecting {}'.format(match.name))
                sel.setCurrentText(match.name)

            self.importMatchTable.setCellWidget(r, 4, sel)

    def clear(self):
        """Clears all import data and the UI"""
        self.ofx = None
        self.imported = None

        self.importMatchTable.setRowCount(0)
        self.ui.importPathTxt.clear()

    def on_browseBtn_clicked(self):
        self.start_import()

    def on_buttonBox_accepted(self):
        """Perform the import"""
        for r in range(self.importMatchTable.rowCount()):
            sel = self.importMatchTable.cellWidget(r, 4)
            local_acc_id = sel.itemData(sel.currentIndex())

            if local_acc_id is not None:
                # If this account doesn't have an account/routing num set, use the one just picked
                local_acc = self.budget.accounts[local_acc_id]
                if not local_acc.routing_number and not local_acc.account_number:
                    local_acc.routing_number = self.imported.accounts[r].routing_number
                    local_acc.account_number = self.imported.accounts[r].account_number
                    self.budget.perform_background_task(local_acc.save)

                # Schedule the import
                #self.imported.accounts[r].perform_background_import(local_acc)
                self.imported.accounts[r].perform_import(local_acc)

        self.clear()

    def on_buttonBox_rejected(self):
        """Cancel the import"""
        self.clear()

    def start_import(self):
        """Prompts the user to select an importable file, showing and hiding the
        dialog based on the response"""
        path = QFileDialog.getOpenFileName(self, "Import Transactions",
            filter="Open File Exchange (*.ofx *.qfx);;Comma-Separated Values (*.csv);;All Files (*.*)")[0]
        if path:
            self.import_from(path)
