import locale

from PyQt5.QtWidgets import QMainWindow, QFileDialog, QMessageBox, QProgressBar
from PyQt5.QtCore import QFile, QIODevice, pyqtSlot
from PyQt5 import uic

from . import *
from ..budget.manager import BudgetManager
from ..budget.errors import BudgetLockedError

class BudgetterMainWindow(QMainWindow):
    def __init__(self):
        super(BudgetterMainWindow, self).__init__()

        # Locale
        locale.setlocale(locale.LC_ALL, '')

        # Open a budget and hook in to it
        self.budget = BudgetManager()
        self.budget.opened.connect(self.on_budget_opened)
        self.budget.closed.connect(self.on_budget_closed)

        # Load UI components XML
        file = QFile(':mainwindow.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open main window UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        # Progress indicator
        self.ui.progressBar = QProgressBar()
        self.ui.statusBar.addWidget(self.ui.progressBar)

        self.__activity_count = 0

        self.set_progress_indefinite()
        self.set_progress_status(0, 'Loading UI...')

        self.budget.activity_start.connect(self.on_activity_start)
        self.budget.activity_progress.connect(self.on_activity_progress)
        self.budget.activity_end.connect(self.on_activity_end)
        self.budget.activity_none.connect(self.on_activity_none)

        #self.login_widget = logintab.LoginTabWidget(self.budget, self.ui.mainTabViews)
        #self.ui.mainTabViews.addTab(self.login_widget, 'Logins')

        self.account_widget = accounttab.AccountTabWidget(self.budget, self.ui.mainTabViews)
        self.ui.mainTabViews.addTab(self.account_widget, 'Accounts')

        self.category_widget = categorytab.CategoryTabWidget(self.budget, self.ui.mainTabViews)
        self.ui.mainTabViews.addTab(self.category_widget, 'Budget')

        self.rules_widget = rulestab.RulesTabWidget(self.budget, self.ui.mainTabViews)
        self.ui.mainTabViews.addTab(self.rules_widget, 'Rules')

        self.transaction_widget = transactiontab.TransactionTabWidget(self.budget, self.ui.mainTabViews)
        self.ui.mainTabViews.addTab(self.transaction_widget, 'Transactions')

        #self.progress = progressdialog.ProgressDialog(self, self.budget)

        # Sync dates on cat and trans tabs
        self.category_widget.ui.toDate.dateChanged.connect(self.on_to_date_changed)
        self.category_widget.ui.fromDate.dateChanged.connect(self.on_from_date_changed)
        self.transaction_widget.ui.toDate.dateChanged.connect(self.on_to_date_changed)
        self.transaction_widget.ui.fromDate.dateChanged.connect(self.on_from_date_changed)
        self.transaction_widget.go_to_newest()

        # Done loading!
        self.clear_progress_status()

    def open_budget(self, path, override_lock=False):
        """Opens the given budget"""
        try:
            self.budget.open(path, override_lock)
        except BudgetLockedError:
            answer = QMessageBox.question(self, 'Budget locked',
                                'This budget is currently locked and may not be opened. If you '
                                'know that you are the only user, you may choose to unlock the '
                                'budget anyways.\n\nUnlock the budget?')
            if answer == QMessageBox.Yes:
                self.budget.open(path, override_lock=True)

    def set_progress_limits(self, min, max):
        self.ui.progressBar.setRange(min, max)

    def set_progress_indefinite(self):
        """Shortcut to make progress bar indefinite (continuous movement)"""
        self.ui.progressBar.show()
        self.set_progress_limits(0, 0)

    def set_progress_status(self, val, text):
        if val is not None:
            self.ui.progressBar.show()
            self.ui.progressBar.setValue(val)

        if text is not None:
            self.statusBar.showMessage(text)

    def clear_progress_status(self):
        self.ui.progressBar.hide()
        self.set_progress_limits(0, 1)
        self.statusBar.clearMessage()

    @pyqtSlot()
    def on_budget_opened(self):
        print('Budget opened')

    @pyqtSlot()
    def on_budget_closed(self):
        print('Budget closed')

    @pyqtSlot()
    @pyqtSlot(int)
    def on_activity_start(self, steps=0):
        self.__activity_count += 1
        self.set_progress_limits(0, steps)
        self.set_progress_status(0, 'Starting')

    @pyqtSlot(int, str)
    def on_activity_progress(self, value, msg):
        """Changes the progress bar to the new value and message. If value is
        None, then the progress bar value is unchanged. Likewise for message.
        To hide the current message, set msg to an empty string."""
        self.set_progress_status(value, msg)

    @pyqtSlot()
    def on_activity_end(self):
        self.__activity_count -= 1
        if self.__activity_count <= 0:
            self.__activity_count = 0
            self.ui.progressBar.hide()

    @pyqtSlot()
    def on_activity_none(self):
        self.__activity_count = 0
        self.ui.progressBar.hide()

    @pyqtSlot(bool)
    def on_fileNewAction_triggered(self, toggle):
        # Where should we save this new budget?
        path = QFileDialog.getSaveFileName(self, "New budget", filter="Budgets (*.budget)")[0]
        if path:
            self.budget.create(path)

    @pyqtSlot(bool)
    def on_fileOpenAction_triggered(self, toggle):
        # Where is the budget?
        path = QFileDialog.getOpenFileName(self, "Open Budget", filter="Budgets (*.budget)")[0]
        if path:
            self.open_budget(path)

    @pyqtSlot(bool)
    def on_fileCloseAction_triggered(self, toggle):
        self.budget.close()

    @pyqtSlot('QDate')
    def on_to_date_changed(self, date):
        # Ensures both category and transaction widget dates stay the same
        self.category_widget.ui.toDate.setDate(date)
        self.transaction_widget.ui.toDate.setDate(date)

    @pyqtSlot('QDate')
    def on_from_date_changed(self, date):
        # Ensures both category and transaction widget dates stay the same
        self.category_widget.ui.fromDate.setDate(date)
        self.transaction_widget.ui.fromDate.setDate(date)
