from PyQt5.QtWidgets import QWidget, QHeaderView, QMenu, QMessageBox
from PyQt5.QtCore import Qt, QFile, QIODevice, QPoint, pyqtSlot
from PyQt5 import uic

from . import ruleeditorwindow
from ..budget.helpers import rules

class RulesTabWidget(QWidget):
    def __init__(self, budget, parent=None):
        super(RulesTabWidget, self).__init__(parent)

        self.__budget = budget

        # Load UI XML
        file = QFile(':rulestab.ui')
        if not file.open(QIODevice.ReadOnly):
            raise IOError('Unable to open rules tab UI from resource file: Error num {}'.format(file.error()))

        self.ui = uic.loadUi(file, self)

        if self.budget.is_open():
            self.enable_tab()
        else:
            self.disable_tab()

        # Prepare editor window
        self.__editor_win = ruleeditorwindow.RuleEditorWindow(self.budget)
        self.__editor_win.hide()

        # Attach to budget
        self.budget.opened.connect(self.enable_tab)
        self.budget.closed.connect(self.disable_tab)

        self.__rules_model = rules.RulesModel(self.budget)
        self.ui.rulesView.setModel(self.__rules_model)
        self.ui.rulesView.verticalHeader().hide()

        # Manage sizing
        headers = self.ui.rulesView.horizontalHeader()
        headers.hideSection(self.__rules_model.id_col)
        headers.hideSection(self.__rules_model.cat_id_col)
        headers.setStretchLastSection(False)
        headers.setSectionResizeMode(QHeaderView.ResizeToContents)
        headers.setSectionResizeMode(self.__rules_model.desc_col, QHeaderView.Stretch)

        # Add menu to allow removal of categories and creation of rules
        self.__menu = QMenu()

        self.__menu.addAction('Add').triggered.connect(self.on_rule_add)
        self.__remove_action = self.__menu.addAction('Remove...')
        self.__remove_action.triggered.connect(self.on_rule_remove)

        self.ui.rulesView.setContextMenuPolicy(Qt.CustomContextMenu)
        self.ui.rulesView.customContextMenuRequested.connect(self.on_show_context_menu)

    @property
    def budget(self):
        return self.__budget

    def enable_tab(self, enabled=True):
        self.ui.rulesView.setEnabled(enabled)

    def disable_tab(self, disabled=True):
        self.enable_tab(not disabled)

    @pyqtSlot('QPoint')
    def on_show_context_menu(self, pos):
        if self.ui.rulesView.selectedIndexes():
            self.__remove_action.setEnabled(True)
        else:
            self.__remove_action.setEnabled(False)

        globalPos = self.ui.rulesView.mapToGlobal(pos)
        menuPos = QPoint(globalPos.x(), globalPos.y() + self.__menu.sizeHint().height() / 2)
        self.__menu.popup(menuPos)

    @pyqtSlot('QModelIndex')
    def on_rulesView_doubleClicked(self, index):
        # Edit the current rule
        rule = self.__rules_model.complete_rule(index)
        self.__editor_win.edit_rule(rule)

    @pyqtSlot('QAction')
    def on_rule_add(self, action):
        self.__editor_win.create_rule()

    @pyqtSlot('QAction')
    def on_rule_remove(self, action):
        # Cache IDs to be removed, because the rows change once we start removing
        to_be_removed = self.__selected_to_ids()

        cnt = len(to_be_removed)

        if cnt == 1:
            answer = QMessageBox.question(self, 'Remove rule?',
                    'Are you sure you want to remove this rule?')
        elif cnt > 1:
            answer = QMessageBox.question(self, 'Remove rules?',
                    'Are you sure you want to remove these {} rules?'.format(cnt))
        else:
            # Nothing was selected?
            return

        if answer == QMessageBox.Yes:
            for id in to_be_removed:
                self.budget.rules[id].remove()

    def __selected_to_ids(self):
        selected = self.ui.rulesView.selectedIndexes()
        ids = list()
        for item in selected:
            ids.append(item.sibling(item.row(), self.__rules_model.id_col).data())

        # Remove duplicates. Needs to be done because selectedIndexes()
        # returns an index for each cell in the rows, not just one per row
        return list(set(ids))
