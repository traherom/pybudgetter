#!/usr/bin/env python3
import sys
import argparse

from PyQt5 import QtWidgets

from pybudgetter.ui.mainwindow import BudgetterMainWindow

def main():
    parser = argparse.ArgumentParser(description='Work with a SQLite-based budget')
    parser.add_argument('--unlock', action='store_true',
                        help='Overrides any existing open lock on the budget')
    parser.add_argument('db', nargs='?', default=None, help='Path to a budget database')
    args = parser.parse_args()

    app = QtWidgets.QApplication(sys.argv)

    win = BudgetterMainWindow()
    win.show()

    if args.db is not None:
        win.open_budget(args.db, override_lock=args.unlock)

    return app.exec_()

if __name__ == '__main__':
    sys.exit(main())
